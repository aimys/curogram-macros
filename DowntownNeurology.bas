Sub Main()

    Columns("H").Replace What:="", Replacement:="1"
    Columns("H").Replace What:="Cancelled", Replacement:=""
    Columns("H").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("J").Select
    Selection.Cut Destination:=Columns("D")

    Columns("J").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Select
    Selection.Cut Destination:=Columns("AB")

    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Cut Destination:=Columns("O")
    Columns("E").Select
    Selection.Cut Destination:=Columns("N")
    Columns("F").Select
    Selection.Cut Destination:=Columns("L")
    Columns("D").Select
    Selection.Cut Destination:=Columns("I")
    Columns("C").Select
    Selection.Cut Destination:=Columns("E")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    Columns("AA").Select
    Selection.Cut Destination:=Columns("F")

    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "P1").Select
    Selection.FillDown

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-2],""hh:mm AM/PM"")"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Columns("P:Q").Select
    Columns("P:Q").Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("O").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-2],"" "",RC[-1])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D").Select
    Selection.Cut Destination:=Columns("C")

    Columns("D").Select
    Selection.ClearContents
    Columns("B").Select
    Selection.ClearContents
    Columns("G").Select
    Selection.ClearContents

    Columns("E:H").Replace What:="-", Replacement:=" "
    Columns("E:H").Replace What:="(", Replacement:="+1 ("

    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Columns("H").ClearContents

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("L2").Select

End Sub
