Sub Main()
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("H").Replace What:=":  ", Replacement:=":"
    Columns("H").Replace What:=": ", Replacement:=":"
    Columns("H").Replace What:=") ", Replacement:=")"
    Columns("H").Replace What:="Cell:E", Replacement:="E"

    Columns("H").Select
    Selection.TextToColumns Destination:=Range("H1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Range("J1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(LEFT(RC[-2],6)=""E-Mail"",RC[-2]=""""),IF(RC[-2]="""","""",RC[-2]),IF(OR(LEFT(RC[-1],6)=""E-Mail"",RC[-1]=""""),IF(RC[-1]="""","""",RC[-1]),""""))"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "J1").Select
    Selection.FillDown
    Selection.Copy
    Selection.Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("I").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(LEFT(RC[-1],6)=""E-Mail"",RC[-1]=""""),"""",RC[-1])"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "I1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft

    Columns("J").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(LEFT(RC[-1],6)=""E-Mail"",RC[-1]=""""),"""",RC[-1])"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "J1").Select
    Selection.FillDown
    Selection.Copy
    Selection.Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("I").Select
    Selection.Delete Shift:=xlToLeft

    Columns("H:I").Replace What:="Cell:", Replacement:=""
    Columns("H:I").Replace What:="Work:", Replacement:=""
    Columns("J").Replace What:="E-Mail:", Replacement:=""

    Columns("A").Select
    Selection.Cut Destination:=Columns("O")
    Columns("D").Select
    Selection.Cut Destination:=Columns("P")
    Columns("G").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("E").Select
    Selection.Cut Destination:=Columns("M")
    Columns("B").Select
    Selection.Cut Destination:=Columns("K")

    Columns("F").Select
    Selection.Cut Destination:=Columns("L")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")

    Columns("A").Replace What:="Mr. ", Replacement:=""
    Columns("A").Replace What:="Mrs. ", Replacement:=""
    Columns("A").Replace What:="Ms. ", Replacement:=""
    Columns("A").Replace What:="Miss. ", Replacement:=""
    Columns("A").Replace What:="Miss ", Replacement:=""
    Columns("A").Replace What:=" )", Replacement:=")"

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""",RC[-2],RC[-1])"
    Range("B1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D").Cut Destination:=Columns("C")

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=RC[-2],"""",RC[-2])"
    Range("B1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D").Cut Destination:=Columns("B")

    Columns("H:I").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("J").Select
    Selection.Cut Destination:=Columns("I")
    Columns("I").Select

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-5]=""NP Consult Child"",RC[-5]=""NP Consult Adult""),""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    
End Sub



