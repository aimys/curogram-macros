Sub Main()

    With ActiveWindow
        .SplitColumn = 0
        .SplitRow = 0
    End With
    
    Rows("1:5").Select
    Selection.Delete Shift:=xlUp
        
    Columns("F:K").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G:H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H:H").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("A:B").Select
    Selection.Cut Destination:=Columns("H:I")
    Columns("D").Select
    Selection.Cut Destination:=Columns("A")
    Columns("E").Select
    Selection.Cut Destination:=Columns("B")
    Columns("C").Select
    Selection.Cut Destination:=Columns("J")
    Columns("I").Select
    Selection.Cut Destination:=Columns("C")
    
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("K").Select
    Selection.Cut Destination:=Columns("F")
    Columns("G").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("J").Select
    Selection.Cut Destination:=Columns("G")

    Range("J1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=""PENDING PATIENT"",""1"","""")"
    Range("I1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "J1").Select
    Selection.FillDown
    Selection.Copy
    Selection.Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("J1").Select
    ActiveCell.FormulaR1C1 = "1"

    Columns("J").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    
    Columns("J").Select
    Selection.TextToColumns Destination:=Range("J1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("I:J").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("D").Select
    Selection.TextToColumns Destination:=Range("D1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 9), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True

    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("C").Select
    Selection.Cut Destination:=Columns("F")
    
    Columns("C").Select
    Selection.Delete Shift:=xlToLeft   

    Columns("H").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "phone"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "email"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "first name"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "middle name"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "last name"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "mapping id"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "charge description"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "amount"
    
End Sub
