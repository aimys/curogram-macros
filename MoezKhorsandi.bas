Attribute VB_Name = "Module1"
Sub Main()

    Columns("B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H:N").Select
    Selection.Delete Shift:=xlToLeft


    Range("H1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=""New Patient Visit"",""true"",""false"")"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "H1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("G").Select
    Selection.Cut Destination:=Columns("U")
    
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("A").Select
    Selection.Cut Destination:=Columns("O")
    Columns("E").Select
    Selection.Cut Destination:=Columns("P")
    Columns("F").Select
    Selection.Cut Destination:=Columns("N")
    Columns("N").Select
    
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    Columns("C:D").Select
    Selection.Cut Destination:=Columns("D:E")
    Columns("G").Select
    Selection.Cut Destination:=Columns("M")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 9), Array(5, 9)), _
        TrailingMinusNumbers:=True
        
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-3]="""","""",IF(RC[-1]="""",RC[-2],RC[-1]))"
    Columns("D").Select
    Selection.FillDown
    Columns("D").Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D:D").Select
    Selection.ClearContents
    
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-3]="""","""",IF(RC[-2]=RC[-1],"""",RC[-2]))"
    Columns("D").Select
    Selection.FillDown
    Columns("D").Copy
    Columns("B").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Columns("E").Select
    Selection.FillDown
    Columns("E").Copy
    Columns("D").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TRIM(SUBSTITUTE(RC[-1],LEFT(RC[-1],3),"""")))"
    Columns("F").Select
    Selection.FillDown
    Columns("F").Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("F").Select
    Selection.ClearContents
    
    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Columns("P").Select
    Selection.FillDown
    Columns("P").Copy
    Columns("O").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("P").Select
    Selection.Delete Shift:=xlToLeft

    'clean up phones
    Columns("E").Replace What:=" ", Replacement:=""
    Columns("E").Replace What:="-", Replacement:=""
    Columns("E").Replace What:="(", Replacement:=""
    Columns("E").Replace What:=")", Replacement:=""

    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-1]="""","""",TEXT(RC[-1],""+1 (###) ### ####""))"
    Columns("F:F").Select
    Selection.FillDown
    Columns("F").Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft

    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O2").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown

    Columns("M").Select
    Selection.Cut Destination:=Columns("R")

    Columns("T").Select
    Selection.Cut Destination:=Columns("M")

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub
