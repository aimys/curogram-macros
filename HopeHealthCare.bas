Sub Main()
Rows("1:19").Select
Selection.Delete Shift:=xlUp

Columns("A").Select
Selection.Delete Shift:=xlToLeft
Columns("E").Select
Selection.Delete Shift:=xlToLeft

Columns("B").Select
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
Columns("A").Select
Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

Columns("C").Select
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
Range("C1").Select
ActiveCell.FormulaR1C1 = "=TRIM(RC[-1])"
Range("A1").Select
Selection.End(xlDown).Select
Range(ActiveCell.Offset(0, 2), "C1").Select
Selection.FillDown
Selection.Copy
Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    :=False, Transpose:=False
Application.CutCopyMode = False

Columns("B").Select
Selection.Delete Shift:=xlToLeft
Columns("C").Select
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
Columns("B").Select
Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
    Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
    :=Array(1, 1), TrailingMinusNumbers:=True
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

Columns("D").Select
Selection.Cut Destination:=Columns("B")
Columns("A").Select
Selection.Cut Destination:=Columns("D")
Columns("C").Select
Selection.Cut Destination:=Columns("A")
Columns("D").Select
Selection.Cut Destination:=Columns("C")
Columns("C").Select

Columns("F").Select
Selection.Cut Destination:=Columns("O")
Columns("O").Select
Selection.Copy
Range("P1").Select
ActiveSheet.Paste
Application.CutCopyMode = False
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
Range("P1").Select
ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
Range("O1").Select
Selection.End(xlDown).Select
Range(ActiveCell.Offset(0, 1), "P1").Select
Selection.FillDown
Selection.Copy
Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    :=False, Transpose:=False
Application.CutCopyMode = False

Columns("O").Select
Selection.Delete Shift:=xlToLeft
Range("Q1").Select
ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",IF(IF(CELL(""contents"",RC[-1])/INT(CELL(""contents"",RC[-1]))=1,TRUE,FALSE),"""",TEXT(RC[-1],""hh:mm AM/PM"")))"
Range("P1").Select
Selection.End(xlDown).Select
Range(ActiveCell.Offset(0, 1), "Q1").Select
Selection.FillDown
Selection.Copy
Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    :=False, Transpose:=False
Application.CutCopyMode = False

Columns("P").Select
Selection.Delete Shift:=xlToLeft

Columns("P").Select
Selection.SpecialCells(xlCellTypeBlanks).Select
Selection.EntireRow.Delete

Columns("E").Select
Selection.Cut Destination:=Columns("K")
Columns("G").Select
Selection.Cut Destination:=Columns("N")
Columns("H").Select
Selection.Cut Destination:=Columns("E")
Columns("E").Select


' clean up phone cloumn
Columns("E").Replace What:="~*", Replacement:=""
Columns("E").Replace What:="Mobile: ", Replacement:=""
Columns("E").Replace What:="Home: ", Replacement:=""
Columns("E").Replace What:="Work: ", Replacement:=""
Columns("E").Replace What:="Other: ", Replacement:=""
Columns("E").Replace What:="Emergency: ", Replacement:=""
Columns("E").Replace What:="Fax: ", Replacement:=""

Range("F1").Select
ActiveCell.FormulaR1C1 = "=CLEAN(SUBSTITUTE(RC[-1],CHAR(10),"",""))"
Range("A1").Select
Selection.End(xlDown).Select
Range(ActiveCell.Offset(0, 5), "F1").Select
Selection.FillDown
Selection.Copy
Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    :=False, Transpose:=False
Application.CutCopyMode = False

Columns("E").Select
Selection.Delete Shift:=xlToLeft
Columns("F").Select
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
Columns("F").Select
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

Columns("E").Select
Selection.TextToColumns Destination:=Range("E1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, OtherChar _
    :=",", FieldInfo:=Array(Array(1, 1), Array(2, 1), Array(3, 1)), _
    TrailingMinusNumbers:=True

Columns("F").Select
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
Range("F1").Select
ActiveCell.FormulaR1C1 = "=LEFT(TRIM(RC[-1]),13)"
Range("A1").Select
Selection.End(xlDown).Select
Range(ActiveCell.Offset(0, 5), "F1").Select
Selection.FillDown
Selection.Copy
Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    :=False, Transpose:=False
Application.CutCopyMode = False
Columns("E").Select
Selection.Delete Shift:=xlToLeft

Columns("G").Select
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
Range("G1").Select
ActiveCell.FormulaR1C1 = "=LEFT(TRIM(RC[-1]),13)"
Range("A1").Select
Selection.End(xlDown).Select
Range(ActiveCell.Offset(0, 6), "G1").Select
Selection.FillDown
Selection.Copy
Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    :=False, Transpose:=False
Application.CutCopyMode = False
Columns("F").Select
Selection.Delete Shift:=xlToLeft

Columns("H").Select
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
Range("H1").Select
ActiveCell.FormulaR1C1 = "=LEFT(TRIM(RC[-1]),13)"
Range("A1").Select
Selection.End(xlDown).Select
Range(ActiveCell.Offset(0, 7), "H1").Select
Selection.FillDown
Selection.Copy
Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    :=False, Transpose:=False
Application.CutCopyMode = False
Columns("G").Select
Selection.Delete Shift:=xlToLeft

Columns("I").Select
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
Range("I1").Select
ActiveCell.FormulaR1C1 = "=LEFT(TRIM(RC[-1]),13)"
Range("A1").Select
Selection.End(xlDown).Select
Range(ActiveCell.Offset(0, 8), "I1").Select
Selection.FillDown
Selection.Copy
Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    :=False, Transpose:=False
Application.CutCopyMode = False
Columns("H").Select
Selection.Delete Shift:=xlToLeft

Columns("I").Select
Selection.Delete Shift:=xlToLeft

'clean up phones
Columns("E:H").Replace What:="-", Replacement:=" "
Columns("E:H").Replace What:="(", Replacement:="+1 ("
Columns("E:H").Replace What:=")", Replacement:=") "

'adding data to duration column
Range("Q1").Activate
ActiveCell.FormulaR1C1 = "30"
Range("O2").End(xlDown).Select
Range(ActiveCell.Offset(0, 2), "Q1").Select
Selection.FillDown

Columns("P").Select
Selection.TextToColumns Destination:=Range("P1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
    Semicolon:=False, Comma:=False, Space:=False, Other:=False, OtherChar _
    :=",", FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

Range("O1").Select
Selection.End(xlDown).Select
Range(ActiveCell.Offset(0, 1), "P1").Select
Selection.SpecialCells(xlCellTypeBlanks).Select
Selection.EntireRow.Delete

' Adding Header Row
Rows("1").Select
Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

' Changing Header Names
ActiveCell.FormulaR1C1 = ""
Range("A1").Select
ActiveCell.FormulaR1C1 = "firstName"
Range("B1").Select
ActiveCell.FormulaR1C1 = "middleName"
Range("C1").Select
ActiveCell.FormulaR1C1 = "lastName"
Range("D1").Select
ActiveCell.FormulaR1C1 = "dob"
Range("E1").Select
ActiveCell.FormulaR1C1 = "phone1"
Range("F1").Select
ActiveCell.FormulaR1C1 = "phone2"
Range("G1").Select
ActiveCell.FormulaR1C1 = "phone3"
Range("H1").Select
ActiveCell.FormulaR1C1 = "phone4"
Range("I1").Select
ActiveCell.FormulaR1C1 = "email1"
Range("J1").Select
ActiveCell.FormulaR1C1 = "email2"
Range("K1").Select
ActiveCell.FormulaR1C1 = "mrn"
Range("L1").Select
ActiveCell.FormulaR1C1 = "address"
Range("M1").Select
ActiveCell.FormulaR1C1 = "service"
Range("N1").Select
ActiveCell.FormulaR1C1 = "physicianName"
Range("O1").Select
ActiveCell.FormulaR1C1 = "appointmentDate"
Range("P1").Select
ActiveCell.FormulaR1C1 = "appointmentTime"
Range("Q1").Select
ActiveCell.FormulaR1C1 = "duration"
Range("L2").Select

End Sub