Sub Main()

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("B:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C:D").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K").Select
    Selection.Delete Shift:=xlToLeft

    Columns("C:D").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("B").Select
    Selection.Cut Destination:=Columns("N")
    Columns("J").Select
    Selection.Cut Destination:=Columns("D")
    Columns("K:L").Select
    Selection.Cut Destination:=Columns("J:K")
    Columns("A").Select
    Selection.Cut Destination:=Columns("L")
    Columns("F").Select
    Selection.Cut Destination:=Columns("M")
    Columns("H").Select
    Selection.Cut Destination:=Columns("A")
    Columns("E").Select
    Selection.Cut Destination:=Columns("R")
    Columns("I:J").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("G").Select
    Selection.Cut Destination:=Columns("J")
    Columns("K").Select
    Selection.Cut Destination:=Columns("G")
    Columns("J").Select
    Selection.Cut Destination:=Columns("K")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    
    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 9), Array(2, 1), Array(3, 1), Array(4, 9)), TrailingMinusNumbers:= _
        True
    
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("A").Select
    Selection.Cut Destination:=Columns("D")

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Range("S1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=""NP"",""true"",""false"")"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "S1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("R").Select
    Selection.Delete Shift:=xlToLeft

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Columns("E:H").Replace What:="(", Replacement:="+1 ("
    Columns("E:H").Replace What:="-", Replacement:=" "

    Columns("D").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub
