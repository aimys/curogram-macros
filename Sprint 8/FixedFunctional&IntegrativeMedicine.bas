Sub Macro1()

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("P:W").Select
    Selection.Delete Shift:=xlToLeft

    Columns("B:C").Select
    Selection.Cut Destination:=Columns("R:S")
    Columns("E").Select
    Selection.Cut Destination:=Columns("U")
    Columns("S").Select
    Selection.Cut Destination:=Columns("T")
    Columns("U").Select
    Selection.Cut Destination:=Columns("V")
    Columns("A").Select
    Selection.Cut Destination:=Columns("E")
    Columns("F:G").Select
    Selection.Cut Destination:=Columns("A:B")
    Columns("B").Select
    Selection.Cut Destination:=Columns("C")

    Columns("D").Select
    Selection.ClearContents

    Columns("H").Select
    Selection.Cut Destination:=Columns("D")

    Columns("E").Select
    Selection.Cut Destination:=Columns("P")
    Columns("K:N").Select
    Selection.Cut Destination:=Columns("E:H")
    Columns("I").Select
    Selection.Cut Destination:=Columns("K")
    Columns("J").Select
    Selection.Cut Destination:=Columns("I")
    Columns("P").Select
    Selection.Cut Destination:=Columns("N")

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("S1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""hh:mmAM/PM"")"
    Range("R1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "S1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("S").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("S1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("R1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "S1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("R").Select
    Selection.Delete Shift:=xlToLeft

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"
End Sub
