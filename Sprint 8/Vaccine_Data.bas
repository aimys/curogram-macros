Sub Main()

    Sheets.Add After:=Sheets(Sheets.Count)
    
    Sheets("Vaccine Data Template").Select
    Cells.Select
    Selection.Copy
    Sheets("Sheet1").Select
    ActiveSheet.Paste
    Application.CutCopyMode = False

    Sheets("Sheet1").Select
    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("M").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("N").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    

    Sheets("Patient Demographics").Select
    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("AC").Select
    Selection.Cut Destination:=Columns("A")
    
    Columns("AC").Select
    Selection.Delete Shift:=xlToLeft

    Sheets("Sheet1").Select
    Rows("1").Select
    Selection.Delete Shift:=xlUp
    
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-1],'Patient Demographics'!R1C1:R5C61,5,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-2],'Patient Demographics'!R1C1:R5C61,3,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "C1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-3],'Patient Demographics'!R1C1:R5C61,8,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-4],'Patient Demographics'!R1C1:R5C61,9,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 4), "E1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-5],'Patient Demographics'!R1C1:R5C61,10,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 5), "F1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("G1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-6],'Patient Demographics'!R1C1:R5C61,33,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 6), "G1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("I1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-8],'Patient Demographics'!R1C1:R5C61,34,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 8), "I1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("J1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-9],'Patient Demographics'!R1C1:R5C61,35,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 9), "J1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("K1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-10],'Patient Demographics'!R1C1:R5C61,36,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 10), "K1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("M1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-12],'Patient Demographics'!R1C1:R5C61,6,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 12), "M1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("N1").Select
    ActiveCell.FormulaR1C1 = "=VLOOKUP(RC[-13],'Patient Demographics'!R1C1:R5C61,54,FALSE)"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 13), "N1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A").Select
    Selection.ClearContents

    Cells.Select
    Selection.ClearFormats

    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""yyyymmdd"")"
    Range("D1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "E1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    Columns("F").Replace What:="Caucasian", Replacement:="White"
    Columns("F").Replace What:="African American", Replacement:="Black or African-American"
    Columns("F").Replace What:="Native American", Replacement:="Value American Indian OR Alaska Native"
    Columns("F").Replace What:="Hawaiian or Pacific Islander", Replacement:="Native Hawaiian or Other Pacific Islander"
    Columns("F").Replace What:="Prefer Not to Say", Replacement:="Prefer not to Specify"

    Columns("N").Replace What:="Hispanic", Replacement:="Hispanic or Latino"

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("A1").Select
    ActiveCell.FormulaR1C1 = "NJIIS_Facility_ID"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "Patient_Last_Name"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "Patient_First_Name"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "Patient_Date_of_Birth_YYYYMMDD"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "Administrative_Sex"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "Race"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "Patient_Address_Street_or_Mailing_Address"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "Patient_Address_line_2"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "Patient_Address_City"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "Patient_Address_State"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "Patient_Address_Zip"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "Patient_Address_Type"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "Phone_Number"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "Ethnic_Group"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "Date_of_Administration_YYYYMMDD"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "Vaccine_Administered_Amount"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "Vaccine_Lot_Number"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "Vaccine_Expiration_Date_YYYYMMDD"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "Vaccine_Manufacturer_Name"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "Vaccine_Route_of_Administration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "Vaccine_Administration_Site"

End Sub
