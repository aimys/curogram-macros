Sub Main()

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H:K").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I:Z").Select
    Selection.Delete Shift:=xlToLeft
    Columns("M:AC").Select
    Selection.Delete Shift:=xlToLeft
    Columns("N").Select
    Selection.Delete Shift:=xlToLeft

    Columns("H").Select
    Selection.Cut Destination:=Columns("N")
    
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft

    Rows("1").Select
    Selection.Delete Shift:=xlUp
    
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""YYYY.mm.dd"")"
    Range("E1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "F1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft

    Range("N1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""YYYY.mm.dd"")"
    Range("M1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "N1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("M").Select
    Selection.Delete Shift:=xlToLeft

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "first_name"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middle_name"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "last_name"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "home_phone"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "date_of_birth"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "sex"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "race"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "address_line_1"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "city"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "state"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "zip"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "ethnicity"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "date_registered"

End Sub
