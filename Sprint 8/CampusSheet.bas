Option Explicit

Private Declare Function URLDownloadToFile Lib "urlmon" _
Alias "URLDownloadToFileA" (ByVal pCaller As Long, _
ByVal szURL As String, ByVal szFileName As String, _
ByVal dwReserved As Long, ByVal lpfnCB As Long) As Long

Dim Ret As Long

'~~> This is where the images will be saved. Change as applicable
Const ParentFolderName As String = "C:\Users\User\Desktop\Curogram\"

Sub Main()

    ActiveSheet.Name = "Sheet1"

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("BC").Select
    Selection.Cut Destination:=Columns("A")
    Columns("BJ").Select
    Selection.Cut Destination:=Columns("B")
    Columns("AA").Select
    Selection.Cut Destination:=Columns("D")
    Columns("W").Select
    Selection.Cut Destination:=Columns("C")

    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("BM").Select
    Selection.Cut Destination:=Columns("E")

    Columns("F:BS").Select
    Selection.Delete Shift:=xlToLeft

    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=SUBSTITUTE(RC[-1],""https://files.curogram.com/"","""")"
    Range("E1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "F1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Dim ws As Worksheet
    Dim LastRow As Long, i As Long
    Dim Folderpath, strPath As String

    Set ws = Sheets("Sheet1")

    LastRow = ws.Range("B" & Rows.Count).End(xlUp).Row

    For i = 1 To LastRow
  
        Folderpath = ParentFolderName & ws.Range("B" & i).Value & "\"
  
        If Len(Dir(Folderpath, vbDirectory)) = 0 Then
            MkDir Folderpath
        End If
  
        strPath = Folderpath & ws.Range("F" & i).Value
        Ret = URLDownloadToFile(0, ws.Range("E" & i).Value, strPath, 0, 0)

        If Ret = 0 Then
            ws.Range("F" & i).Value = "File successfully downloaded"
        Else
            ws.Range("F" & i).Value = "Unable to download the file"
        End If

    Next i

    Columns("E:F").Select
    Selection.Delete Shift:=xlToLeft

    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "CampusID"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "Email"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "TestDatetime"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "TestLocation"

End Sub

