Sub Main()

    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("B2").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("A2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B2").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("C2").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-2],""hh:mmAM/PM"")"
    Range("B2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "C2").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("O").Select
    Selection.Delete Shift:=xlToLeft
    Columns("P:AA").Select
    Selection.Delete Shift:=xlToLeft

    Range("P2").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("O2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "P2").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("O").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A:B").Select
    Selection.Cut Destination:=Columns("R:S")
    Columns("F").Select
    Selection.Cut Destination:=Columns("A")
    Columns("G").Select
    Selection.Cut Destination:=Columns("B")
    Columns("J").Select
    Selection.Cut Destination:=Columns("F")
    Columns("K").Select
    Selection.Cut Destination:=Columns("G")
    Columns("H").Select
    Selection.Cut Destination:=Columns("K")
    Columns("L").Select
    Selection.Cut Destination:=Columns("H")
    Columns("M").Select
    Selection.Cut Destination:=Columns("J")
    Columns("C").Select
    Selection.Cut Destination:=Columns("M")
    Columns("E").Select
    Selection.Cut Destination:=Columns("C")
    Columns("I").Select
    Selection.Cut Destination:=Columns("E")
    Columns("J").Select
    Selection.Cut Destination:=Columns("I")
    Columns("D").Select
    Selection.Cut Destination:=Columns("P")
    Columns("O").Select
    Selection.Cut Destination:=Columns("D")
    Columns("P").Select
    Selection.Cut Destination:=Columns("O")
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("T1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("S1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "T1").Select
    Selection.FillDown

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-7]=""PHA New Pt"",""true"",""false"")"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("H").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("J").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("L").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEN(RC[-1])>10,"""",RC[-1])"
    Range("E1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "F1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("H1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEN(RC[-1])>10,"""",RC[-1])"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "H1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("J1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEN(RC[-1])>10,"""",RC[-1])"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 4), "J1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("L1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEN(RC[-1])>10,"""",RC[-1])"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 6), "L1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E:H").Replace What:="0", Replacement:="", LookAt:=xlWhole
    Columns("E:H").Replace What:="1111111111", Replacement:="", LookAt:=xlWhole

    Range("V1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-8]=""Reschedule"",RC[-8]=""cancel""),RC[-8],"""")"
    Range("U1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "V1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("N").Replace What:="Reschedule", Replacement:="", LookAt:=xlWhole
    Columns("N").Replace What:="cancel", Replacement:="", LookAt:=xlWhole

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"
    
End Sub
