Sub Main()

    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("F").Select
    Selection.Delete Shift:=xlToLeft

    Columns("G").Select
    Selection.Delete Shift:=xlToLeft

    Columns("M").Select
    Selection.Cut Destination:=Columns("AA")

    Columns("L:N").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("F").Select
    Selection.Cut Destination:=Columns("A")

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 9), Array(1, 1)), TrailingMinusNumbers:=True

    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("B").Select
    Selection.Cut Destination:=Columns("C")
    Columns("D:F").Select
    Selection.Cut Destination:=Columns("R:T")
    
    Columns("H").Select
    Selection.Cut Destination:=Columns("D")
    Columns("M").Select
    Selection.Cut Destination:=Columns("E")
    
    Columns("J:K").Select
    Selection.Cut Destination:=Columns("F:G")
    
    Columns("O").Select
    Selection.Cut Destination:=Columns("K")
    Columns("N").Select
    Selection.Cut Destination:=Columns("O")
    Columns("I").Select
    Selection.Cut Destination:=Columns("N")
    Columns("L").Select
    Selection.Cut Destination:=Columns("I")

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-7]=""D New Patient Exam"",RC[-7]=""New Patient""),""true"",""false"")"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D").Replace What:="/", Replacement:="."
    Columns("R").Replace What:="/", Replacement:="."
    Columns("S").Replace What:=" ", Replacement:=""
    Columns("S").Replace What:=":00AM", Replacement:="AM"
    Columns("S").Replace What:=":00PM", Replacement:="PM"

    Columns("AA").Select
    Selection.Cut Destination:=Columns("V")

    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-4],"" "",RC[-3],"" "",RC[-2],"" "",RC[-1])"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "F1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B:E").Select
    Selection.Delete Shift:=xlToLeft

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

End Sub
