Sub Main()

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I").Select
    Selection.Delete Shift:=xlToLeft

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 9), Array(18, 1)), TrailingMinusNumbers:=True

    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 9), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("B:C").Select
    Selection.Cut Destination:=Columns("R:S")
    Columns("A").Select
    Selection.Cut Destination:=Columns("M")
    Columns("D").Select
    Selection.Cut Destination:=Columns("A")
    Columns("H").Select
    Selection.Cut Destination:=Columns("N")
    Columns("G").Select
    Selection.Cut Destination:=Columns("O")
    Columns("E").Select
    Selection.Cut Destination:=Columns("K")
    Columns("F").Select
    Selection.Cut Destination:=Columns("E")

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("A:C")

    Columns("R").Replace What:="/", Replacement:="."

    Range("T1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("S1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "T1").Select
    Selection.FillDown

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-7]=""NEW CONSULT"",""true"",""false"")"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("W1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(SUBSTITUTE(RC[-5],""."",""/""),""dddd"")"
    Range("U1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "W1").Select
    Selection.FillDown

    Range("X1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-1]=""Monday"",RC[-1]=""Thursday""),""Wilmington Location"",IF(OR(RC[-1]=""Tuesday"",RC[-1]=""Wednesday""),""Springfield Location"",IF(RC[-1]=""Friday"",""DR STEVEN GROSSINGER"","""")))"
    Range("W1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "X1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("W").Select
    Selection.Delete Shift:=xlToLeft

    Columns("M").Select
    Selection.ClearContents

    Columns("W").Select
    Selection.Cut Destination:=Columns("M")

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

End Sub