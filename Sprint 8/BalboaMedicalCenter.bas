Sub Main()
    
    Columns("J").Select
    Selection.Cut Destination:=Columns("AZ")

    Columns("J:K").Select
    Selection.Delete Shift:=xlToLeft
   
    Columns("H").Select
    Selection.Cut Destination:=Columns("L")
    Columns("I").Select
    Selection.Cut Destination:=Columns("M")
    Columns("B").Select
    Selection.Cut Destination:=Columns("N")
    Columns("A").Select
    Selection.Cut Destination:=Columns("O")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("G").Select
    Selection.Cut Destination:=Columns("I")
    Columns("D:F").Select
    Selection.Cut Destination:=Columns("E:G")

    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "P1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-2],""hh:mm AM/PM"")"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("O").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Replace What:="#", Replacement:=""
    Columns("A").Replace What:="2", Replacement:=""
    Columns("A").Replace What:="8", Replacement:=""
    Columns("A").Replace What:="3", Replacement:=""
    Columns("A").Replace What:="6", Replacement:=""

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    Columns("B").Select
    Selection.Cut Destination:=Columns("D")
    Columns("C:D").Select
    Selection.Cut Destination:=Columns("B:C")

    Columns("B").Replace What:=",", Replacement:=""
    Columns("B").Replace What:=".", Replacement:=""

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Columns("O:Q").Select
    Selection.Cut Destination:=Columns("R:T")
    Columns("AW").Select
    Selection.Cut Destination:=Columns("V")
    Columns("L:N").Select
    Selection.Cut Destination:=Columns("M:O")

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

End Sub