Sub Macro1()

    Columns("O").Select
    Selection.Cut Destination:=Columns("AZ")

    Columns("B:D").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J:U").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K:Q").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L:M").Select
    Selection.Delete Shift:=xlToLeft
    Columns("M:O").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Cut Destination:=Columns("R")
    Columns("J").Select
    Selection.Cut Destination:=Columns("S")
    Columns("G").Select
    Selection.Cut Destination:=Columns("T")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    Columns("H").Select
    Selection.Cut Destination:=Columns("J")
    Columns("L").Select
    Selection.Cut Destination:=Columns("O")
    Columns("K").Select
    Selection.Cut Destination:=Columns("M")
    Columns("J").Select
    Selection.Cut Destination:=Columns("K")
    Columns("I").Select
    Selection.Cut Destination:=Columns("N")
    
    Columns("N").Select
    Selection.ClearContents
    
    Columns("E").Select
    Selection.Cut Destination:=Columns("N")
    Columns("D").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("C").Select
    Selection.Cut Destination:=Columns("D")
    Columns("F").Select
    Selection.Cut Destination:=Columns("E")
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("W").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("W").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :=".", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(TRIM(RC[-2]),"" "",TRIM(RC[-1]))"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -2), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B:C").Select
    Selection.ClearContents
    
    Columns("D").Select
    Selection.Cut Destination:=Columns("E")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, OtherChar _
        :=".", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=IF(TRIM(RC[-1])="""",CONCATENATE(TRIM(RC[-4]),"" "",TRIM(RC[-3]),"" "",TRIM(RC[-2])),TRIM(RC[-1]))"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "F1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=IF(TRIM(RC[-1])="""","""",TRIM(RC[-4]))"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "F1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B:E").Select
    Selection.Delete Shift:=xlToLeft

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-7]=""New Patient"",""true"",""false"")"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("S").Select
    Selection.TextToColumns Destination:=Range("S1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, OtherChar _
        :=".", FieldInfo:=Array(Array(1, 9), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("R").Replace What:="/", Replacement:="."
    Columns("D").Replace What:="/", Replacement:="."
    
    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub
