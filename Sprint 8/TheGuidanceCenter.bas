Sub Main()

    Columns("B:D").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D:E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Selection.Delete Shift:=xlToLeft
    Columns("I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("F").Select
    Selection.Cut Destination:=Columns("J")
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("K1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEN(RC[-1])=3,CONCATENATE(""0"",RC[-1]),RC[-1])"
    Range("J1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "K1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("J").Select
    Selection.Delete Shift:=xlToLeft

    Columns("J").Select
    Selection.TextToColumns Destination:=Range("J1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Range("L1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-2],"":"",RC[-1])"
    Range("K1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "L1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("M1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""hh:mmAM/PM"")"
    Range("L1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "M1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("J:L").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("E").Select
    Selection.Cut Destination:=Columns("K")

    Columns("K").Select
    Selection.TextToColumns Destination:=Range("K1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 1), Array(4, 1), Array(6, 1)), TrailingMinusNumbers:= _
        True

    Range("N1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-2],""."",RC[-1],""."",RC[-3])"
    Range("M1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "N1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("K:M").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("J").Select
    Selection.Cut Destination:=Columns("L")
    Columns("K:L").Select
    Selection.Cut Destination:=Columns("R:S")
    Columns("I").Select
    Selection.Cut Destination:=Columns("V")
    Columns("D").Select
    Selection.Cut Destination:=Columns("P")
    Columns("A").Select
    Selection.Cut Destination:=Columns("K")
    Columns("B").Select
    Selection.Cut Destination:=Columns("D")
    
    Columns("C").Select
    Selection.Copy
    Columns("E").Select
    ActiveSheet.Paste
    Application.CutCopyMode = False

    Columns("G:H").Select
    Selection.Cut Destination:=Columns("N:O")
    Columns("D").Select
    Selection.Cut Destination:=Columns("W")

    Columns("W").Select
    Selection.TextToColumns Destination:=Range("W1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 1), Array(4, 1), Array(6, 1)), TrailingMinusNumbers:= _
        True

    Range("Z1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-2],""."",RC[-1],""."",RC[-3])"
    Range("Y1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Z1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("W:Y").Select
    Selection.Delete Shift:=xlToLeft
    Columns("W").Select
    Selection.Cut Destination:=Columns("D")

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

End Sub
