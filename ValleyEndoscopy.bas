Sub Main()
    'Deleting Header Row
    Rows("1").Select
    Selection.Delete Shift:=xlUp
    
    'Deleting Extra Columns
    Columns("A:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("B:C").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft
    
    'Moving Physician Name Column
    Columns("A").Select
    Selection.Cut Destination:=Columns("N")
    
    'Moving Appointment Date and Time Columns
    Columns("B:C").Select
    Selection.Cut Destination:=Columns("O:P")
    
    'Moving Name Column
    Columns("E").Select
    Selection.Cut Destination:=Columns("A")
    
    'Moving Phone Column
    Columns("F").Select
    Selection.Cut Destination:=Columns("E")
    
    'Moving mrn Column
    Columns("D").Select
    Selection.Cut Destination:=Columns("K")

    Columns("G").Select
    Selection.Cut Destination:=Columns("D")

    Columns("H").Select
    Selection.Cut Destination:=Columns("F")

    ' Adding Header Row
    Rows("1:1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    'Spliting Name Column to FirstName and LastName
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    
    'Triming LastName Column
    Columns("B").Select
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=TRIM(RC[-1])"
    Columns("C").Select
    Selection.FillDown
    Columns("C").Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("B").Select
    Selection.ClearContents
    Columns("C").Select
    Selection.Cut Destination:=Columns("B")

    'Splitting Lastname column to get middle name
    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    
    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("A").Select
    Selection.Cut Destination:=Columns("C")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    Columns("D").Select
    Selection.Cut Destination:=Columns("B")
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    
    'Formatting Date Column
    Columns("O").Replace What:="/", Replacement:="."
    Columns("D").Replace What:="/", Replacement:="."

    'Formatting Time Column
    Columns("Q").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""hh:mm AM/PM""))"
    Columns("Q").Select
    Selection.FillDown
    Columns("Q").Copy
    Columns("Q").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("P").Select
    Selection.Delete Shift:=xlToLeft
    
    'clean up phones
    Columns("E:F").Replace What:=" ", Replacement:=""
    Columns("E:F").Replace What:="-", Replacement:=""
    Columns("E:F").Replace What:="(", Replacement:=""
    Columns("E:F").Replace What:=")", Replacement:=""

    'Formatting Phone Columns
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-3]="""","""",TEXT(RC[-1],""+1 (###) ### ####""))"
    Columns("F").Select
    Selection.FillDown
    Columns("F").Select
    Columns("F").Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    
    'Formatting Phone Columns
    Columns("G").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("G1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-4]="""","""",TEXT(RC[-1],""+1 (###) ### ####""))"
    Columns("G").Select
    Selection.FillDown
    Columns("G").Select
    Columns("G").Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft

    Columns("N").Select
    Selection.ClearContents
    
    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 9), Array(3, 9)), TrailingMinusNumbers:=True

    Columns("P").Replace What:=" ", Replacement:=""

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("L2").Select
    
    'adding data to duration column
    Range("Q2").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q2").Select
    Selection.FillDown
    
End Sub



