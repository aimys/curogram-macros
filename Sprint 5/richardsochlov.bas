Sub Main()

    ActiveWindow.DisplayGridlines = True
    
    Cells.Select
    Selection.Font.Bold = False
    With Selection.Font
        .Name = "ARIAL"
        .Size = 11
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ColorIndex = 1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontNone
    End With

    Cells.Select
    Selection.UnMerge
    
    Columns("I").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    
    Rows("2").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireColumn.Delete
    
    Cells.Select
    Cells.EntireColumn.AutoFit
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[1]=""New Pt"",""true"",""false"")"
    Range("D1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "E1").Select
    Selection.FillDown
    Selection.Copy
    Columns("V").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Select
    Selection.Cut Destination:=Columns("O")
    
    Columns("E:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D").Select
    Selection.Cut Destination:=Columns("K")
    Columns("B").Select
    Selection.Cut Destination:=Columns("O")
    Columns("A").Select
    Selection.Cut Destination:=Columns("N")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    
    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 9), Array(2, 1)), TrailingMinusNumbers:=True
        
    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("A:C")
    
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]="""","""",TEXT(RC[-2],""hh:mm AM/PM""))"
    
    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "P1").Select
    Selection.FillDown
    Columns("P:Q").Copy
    Columns("O:P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("Q").Select
    Selection.Delete Shift:=xlToLeft

    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O2").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown

    'clean up phones
    Columns("E").Replace What:="-", Replacement:=" "
    Columns("E").Replace What:="(", Replacement:="+1 ("

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")


    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    
End Sub
