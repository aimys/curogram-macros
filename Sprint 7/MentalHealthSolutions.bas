Sub Main()

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K:M").Select
    Selection.Delete Shift:=xlToLeft
    Columns("P:Y").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("Q").Select
    Selection.Cut Destination:=Columns("A")
    Columns("T").Select
    Selection.Cut Destination:=Columns("D")
    Columns("R").Select
    Selection.Cut Destination:=Columns("E")
    Columns("W:X").Select
    Selection.Cut Destination:=Columns("F:G")

    Columns("J").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("V:W").Select
    Selection.Cut Destination:=Columns("I:J")

    Columns("R:S").Select
    Selection.Delete Shift:=xlToLeft

    Columns("M").Select
    Selection.ClearContents

    Columns("Q").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Delete Shift:=xlToLeft

    Columns("L").Select
    Selection.Cut Destination:=Columns("R")
    Columns("N").Select
    Selection.Cut Destination:=Columns("S")
    Columns("O").Select
    Selection.Cut Destination:=Columns("T")
    Columns("P").Select
    Selection.Cut Destination:=Columns("O")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("R").Select
    Selection.TextToColumns Destination:=Range("R1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("S").Select
    Selection.Cut Destination:=Columns("T")

    Columns("R").Select
    Selection.TextToColumns Destination:=Range("R1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 9), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-2],""-"",RC[-3],""-"",RC[-1])"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("V1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("U1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "V1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("Q:U").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 1), Array(2, 9)), TrailingMinusNumbers:=True

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :=".", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.Cut Destination:=Columns("C")

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, OtherChar _
        :=".", FieldInfo:=Array(Array(1, 1), Array(2, 1), Array(3, 9)), _
        TrailingMinusNumbers:=True

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(TRIM(RC[-1])="""",RC[-2],TRIM(RC[-1]))"
    Range("E1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D:D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=RC[-2],"""",RC[-2])"
    Range("E1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.ClearContents

    Columns("D").Select
    Selection.Cut Destination:=Columns("B")

    Columns("T").Replace What:="m", Replacement:=""

    Columns("L").Select
    Selection.Delete Shift:=xlToLeft

    Columns("N").Select
    Selection.Copy
    Columns("U").Select
    ActiveSheet.Paste
    Application.CutCopyMode = False

    Columns("U").Replace What:="90791.1T", Replacement:="TRUE", LookAt:=xlWhole
    Columns("U").Replace What:="90791.1", Replacement:="TRUE", LookAt:=xlWhole
    Columns("U").Replace What:="90791.2T", Replacement:="TRUE", LookAt:=xlWhole
    Columns("U").Replace What:="90791.2", Replacement:="TRUE", LookAt:=xlWhole
    Columns("U").Replace What:="90792T", Replacement:="TRUE", LookAt:=xlWhole
    Columns("U").Replace What:="90792", Replacement:="TRUE", LookAt:=xlWhole
    Columns("U").Replace What:="90837T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90837H", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90837", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90847T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90847.5T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90847.4T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90847.4", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90847", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="99213T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="99213.60T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="99213.6", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="99213", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="99214T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="99214", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="Cancel by Pt", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90834", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="No Show Eval", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90846T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="Self Pay 1002T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90832T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="Late Cancel 1", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="No Show 3", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="Cancel by Prov", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90837E", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90832T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90834T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90847.4T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90847.5T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="90846T", Replacement:="FALSE", LookAt:=xlWhole
    Columns("U").Replace What:="99213.60T", Replacement:="FALSE", LookAt:=xlWhole

    Columns("N").Replace What:="90791.1T", Replacement:="Telemedicine PhD Diagnostic Eval", LookAt:=xlWhole
    Columns("N").Replace What:="90791.1", Replacement:="PhD Diagnostic Evaluation", LookAt:=xlWhole
    Columns("N").Replace What:="90791.2T", Replacement:="Telemedicine Master's Level Diag Eval", LookAt:=xlWhole
    Columns("N").Replace What:="90791.2", Replacement:="Master's Level Diagnostic Evaluation", LookAt:=xlWhole
    Columns("N").Replace What:="90792T", Replacement:="Telemedicine Psych Diag Eval w/ Med Svcs", LookAt:=xlWhole
    Columns("N").Replace What:="90792", Replacement:="Psychiatric Diagnostic Eval. w/ Med Svcs", LookAt:=xlWhole
    Columns("N").Replace What:="90837T", Replacement:="Telemedicine Psychotherapy (60 Minutes)", LookAt:=xlWhole
    Columns("N").Replace What:="90837", Replacement:="Telemedicine Psych Diag Eval w/ Med Svcs", LookAt:=xlWhole
    Columns("N").Replace What:="90847T", Replacement:="Telemedicine Family Counseling", LookAt:=xlWhole
    Columns("N").Replace What:="90847.4T", Replacement:="Telemedicine Couples Psychotherapy", LookAt:=xlWhole
    Columns("N").Replace What:="90847.4", Replacement:="Couples Psychotherapy - 195", LookAt:=xlWhole
    Columns("N").Replace What:="90847", Replacement:="Family Counseling", LookAt:=xlWhole
    Columns("N").Replace What:="99213T", Replacement:="Telemedicine Est Pt Low Complexity", LookAt:=xlWhole
    Columns("N").Replace What:="99213", Replacement:="Est Pt Low Complexity", LookAt:=xlWhole
    Columns("N").Replace What:="99214T", Replacement:="Telemedicine Est Pt Moderate Complexity", LookAt:=xlWhole
    Columns("N").Replace What:="99214", Replacement:="Est Pt Moderate Complexity", LookAt:=xlWhole
    Columns("N").Replace What:="90837E", Replacement:="Psychotherapy - EMDR (60 Minutes)", LookAt:=xlWhole
    Columns("N").Replace What:="90847.4T", Replacement:="Telemedicine Couples Psychotherapy", LookAt:=xlWhole
    Columns("N").Replace What:="90847.5T", Replacement:="Telemedicine Couples Psychoth -Self-Pay", LookAt:=xlWhole
    Columns("N").Replace What:="90846T", Replacement:="Telemedicine Fam Counseling w/o Pt", LookAt:=xlWhole
    Columns("N").Replace What:="99213.60T", Replacement:="Telemedicine Est Pt Low Complex 60 Min", LookAt:=xlWhole
    

    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("K").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("U").Select
    Selection.Cut Destination:=Columns("T")

    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("D").Replace What:="/", Replacement:="."

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

End Sub
