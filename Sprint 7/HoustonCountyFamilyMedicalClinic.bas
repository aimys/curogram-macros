Sub Main()
    
    Columns("B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E:I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F:O").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K:L").Select
    Selection.Delete Shift:=xlToLeft
    Columns("M").Select
    Selection.Delete Shift:=xlToLeft
    Columns("N:P").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Cut Destination:=Columns("R")
    
    Columns("R").Select
    Selection.TextToColumns Destination:=Range("R1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="-", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("I").Select
    Selection.Cut Destination:=Columns("T")
    Columns("H").Select
    Selection.Cut Destination:=Columns("N")
    Columns("L").Select
    Selection.Cut Destination:=Columns("V")

    Columns("J").Select
    Selection.ClearContents
    
    Columns("K").Select
    Selection.Cut Destination:=Columns("O")
    Columns("E").Select
    Selection.Cut Destination:=Columns("I")
    Columns("B").Select
    Selection.Cut Destination:=Columns("K")
    Columns("F:G").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, OtherChar _
        :="-", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.Cut Destination:=Columns("C")
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp
    
    Columns("F").Select
    Selection.TextToColumns Destination:=Range("F1"), DataType:=xlFixedWidth, _
        OtherChar:="-", FieldInfo:=Array(Array(0, 1), Array(10, 9)), _
        TrailingMinusNumbers:=True

    Columns("R").Replace What:="/", Replacement:="."
    Columns("D").Replace What:="/", Replacement:="."

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

End Sub
