Sub Main()

    Columns("A:C").Select
    Selection.Cut Destination:=Columns("R:T")
    Columns("E").Select
    Selection.Cut Destination:=Columns("A")
    Columns("H").Select
    Selection.Cut Destination:=Columns("M")
    Columns("D").Select
    Selection.Cut Destination:=Columns("O")
    Columns("G").Select
    Selection.Cut Destination:=Columns("N")
    Columns("F").Select
    Selection.Cut Destination:=Columns("D")
    Columns("K").Select
    Selection.Cut Destination:=Columns("E")
    Columns("J").Select
    Selection.Cut Destination:=Columns("V")

    Columns("I").Select
    Selection.ClearContents

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("D").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=True, Other:=False, FieldInfo:= _
        Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 9)), TrailingMinusNumbers:=True
    
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("A").Select
    Selection.Cut Destination:=Columns("D")

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Replace What:="H/C", Replacement:="C"

    Columns("E").Select
    Selection.TextToColumns Destination:=Range("E1"), DataType:=xlFixedWidth, OtherChar:=":", FieldInfo:=Array(Array(0, 9), Array(3, 1), Array(17, 9), Array(20, 1)), TrailingMinusNumbers:=True

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-7]=""NP"",""true"",""false"")"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("R").Replace What:="/", Replacement:="."
    Columns("D").Replace What:="/", Replacement:="."

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

End Sub
