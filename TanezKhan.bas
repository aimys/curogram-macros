Sub Main()
    Cells.Select
    Selection.ClearFormats
    Selection.Font.Size = 10
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    
    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("B1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(LEFT(RC[-1],1) = ""D"",RIGHT(LEFT(RC[-1],16),10),"""")"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B1").Select
    Selection.FillDown
    Columns("B").Copy
    Columns("B").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
        
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B1").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Selection.FillDown
    Columns("B").Select
    Columns("B").Copy
    Columns("B").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("C").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("A").Replace What:="a", Replacement:=" AM"
    Columns("A").Replace What:="p", Replacement:=" PM"

    Columns("F:H").Replace What:="(Cell)", Replacement:=""
    Columns("F:H").Replace What:="(Home)", Replacement:=""
    Columns("F:H").Replace What:="(Work)", Replacement:=""
    Columns("F:H").Replace What:="x126", Replacement:=""
    Columns("F:H").Replace What:="-", Replacement:=""
    Columns("F:H").Replace What:="()", Replacement:=""

    Columns("I:K").Select
    Selection.Delete Shift:=xlToRight

    Columns("C").Select
    Selection.Cut Destination:=Columns("K")
    Columns("A:B").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("O").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("P:Q").Select
    Selection.Cut Destination:=Columns("O:P")
        
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")
    Columns("E").Select
    Selection.Cut Destination:=Columns("A")
    Columns("F").Select
    Selection.Cut Destination:=Columns("E")
    Columns("G").Select
    Selection.Cut Destination:=Columns("F")
    Columns("H").Select
    Selection.Cut Destination:=Columns("G")
    Columns("G").Select

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 9), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True
    
    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    

    'Formatting Phone Columns
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-3]="""","""",TEXT(RC[-1],""+1 (###) ### ####""))"
    Columns("F").Select
    Selection.FillDown
    Columns("F").Select
    Columns("F").Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    
    'Formatting Phone Columns
    Columns("G").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("G1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-4]="""","""",TEXT(RC[-1],""+1 (###) ### ####""))"
    Columns("G").Select
    Selection.FillDown
    Columns("G").Select
    Columns("G").Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft

    'Formatting Phone Columns
    Columns("H").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("H1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-5]="""","""",TEXT(RC[-1],""+1 (###) ### ####""))"
    Columns("H").Select
    Selection.FillDown
    Columns("H").Select
    Columns("H").Copy
    Columns("G").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E:H").Replace What:="+1 ()", Replacement:=""
    
    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("L2").Select

    'adding data to duration column
    Range("Q2").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q2").Select
    Selection.FillDown
    
End Sub
