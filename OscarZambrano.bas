Sub Main()
    'Deleting Header Row
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("T").Select
    Selection.Cut Destination:=Columns("AY")

    Columns("T:U").Select
    Selection.Delete Shift:=xlToLeft

    Columns("P").Select
    Selection.Delete Shift:=xlToLeft

    Columns("N").Select
    Selection.Copy
    Columns("M").PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("M").Replace What:="DEPO", Replacement:=""
    Columns("M").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("N").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=""NP"",""true"",""false"")"
    Range("M1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "N1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("M").Select
    Selection.Cut Destination:=Columns("AZ")
 
    'Deleting Extra Columns
    Columns("A:E").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Cut Destination:=Columns("AX")

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Columns("B:C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Cut Destination:=Columns("R")
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("M").Select
    Selection.Cut Destination:=Columns("R")
    
    'Moving Physician Name Column
    Columns("A").Select
    Selection.Cut Destination:=Columns("N")
    
    'Moving Appointment Date and Time Columns
    Columns("B:C").Select
    Selection.Cut Destination:=Columns("O:P")

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    'Moving Name Column
    Columns("E").Select
    Selection.Cut Destination:=Columns("A")
    
    'Moving Phone Column
    Columns("F").Select
    Selection.Cut Destination:=Columns("E")

    Columns("H").Select
    Selection.Cut Destination:=Columns("F")
    
    'Moving mrn Column
    Columns("D").Select
    Selection.Cut Destination:=Columns("K")

    Columns("G").Select
    Selection.Cut Destination:=Columns("D")

    
    
    'Spliting Name Column to FirstName and LastName
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    
    'Triming LastName Column
    Columns("B").Select
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=TRIM(RC[-1])"
    Columns("C").Select
    Selection.FillDown
    Columns("C").Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("B").Select
    Selection.ClearContents
    Columns("D:F").Select
    Selection.Cut Destination:=Columns("G:I")

    'Splitting Lastname column to get middle name
    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    Columns("F").Select
    Selection.Cut Destination:=Columns("E")

    'Moving Middle Name Column
    Columns("D").Select
    Selection.Cut Destination:=Columns("B")

    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")
    
    Columns("G:I").Select
    Selection.Cut Destination:=Columns("D:F")

    'Formatting Date Column
    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Columns("P").Select
    Selection.FillDown
    Columns("P").Copy
    Columns("P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("O").Select
    Selection.Delete Shift:=xlToLeft

    'Formatting Time Column
    Columns("Q").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""hh:mm AM/PM""))"
    Columns("Q").Select
    Selection.FillDown
    Columns("Q").Copy
    Columns("Q").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("P").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(1, 1), TrailingMinusNumbers:=True
    
    Columns("P").Select
    Selection.TextToColumns Destination:=Range("P1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("D").Replace What:="/", Replacement:="."

    'clean up phones
    Columns("E:H").Replace What:="-", Replacement:=""
    Columns("E:H").Replace What:="(", Replacement:=""
    Columns("E:H").Replace What:=")", Replacement:=""
    Columns("E:H").Replace What:=" ", Replacement:=""

    'Formatting Phone1 Columns
    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],""-"",""""),""+1 (###) ### ####""))"
    Columns("E").Select
    Selection.FillDown
    Selection.Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("E").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Formatting Phone2 Columns
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],""-"",""""),""+1 (###) ### ####""))"
    Columns("F").Select
    Selection.FillDown
    Selection.Copy
    Columns("G").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("F").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft

    Columns("AP").Select
    Selection.ClearContents

    Columns("AJ").Select
    Selection.Cut Destination:=Columns("S")

    Columns("AM").Select
    Selection.Cut Destination:=Columns("M")

    Columns("S").Replace What:="Spanish", Replacement:="ESL"
    Columns("S").Replace What:="English", Replacement:="ENG"
    
    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "language"

    'adding data to duration column
    Range("Q2").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q2").Select
    Selection.FillDown
    
End Sub










