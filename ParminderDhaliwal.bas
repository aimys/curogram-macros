Sub Main()

    Columns("I:N").Select
    Selection.Delete Shift:=xlToLeft

    Range("I1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=""New Patient Visit"",""true"",""false"")"
    Range("H1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "I1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("H").Select
    Selection.Cut Destination:=Columns("V")

    Columns("H").Select
    Selection.Delete Shift:=xlToLeft
    Selection.Cut Destination:=Columns("R")
    Columns("F").Select
    Selection.Cut Destination:=Columns("P")
    Columns("G").Select
    Selection.Cut Destination:=Columns("N")
    Columns("A").Select
    Selection.Cut Destination:=Columns("O")

    Columns("B").Select
    Selection.ClearContents

    Columns("C").Select
    Selection.Cut Destination:=Columns("A")

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True

    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]="""",RC[-3],RC[-2])"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "E1").Select
    Selection.FillDown
    Selection.Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("E").Select
    Selection.ClearContents

    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]=RC[-3],"""",RC[-3])"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "E1").Select
    Selection.FillDown
    Selection.Copy
    Columns("B").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("E").Select
    Selection.ClearContents

    Range("E1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-1]="""",RC[-2],CONCATENATE(RC[-2],"" "",RC[-1]))"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "E1").Select
    Selection.FillDown
    Selection.Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D:E").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Replace What:="M. ", Replacement:=""
    Columns("E").Replace What:="(", Replacement:=""
    Columns("E").Replace What:=")", Replacement:=""
    Columns("E").Replace What:="-", Replacement:=""
    Columns("E").Replace What:=" ", Replacement:=""

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    Range("F1").Select
    ActiveCell.FormulaR1C1 = _
        "=TEXT(RC[-1],""+1 (###) ### ####"")"
    Range("D1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "F1").Select
    Selection.FillDown
    Selection.Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("F").Select
    Selection.ClearContents

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("R1").End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "Q1").Select
    Selection.FillDown

    Columns("U").Select
    Selection.Cut Destination:=Columns("M")

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    
End Sub
