Sub Main()
    Columns("P").Select
    Selection.Cut Destination:=Columns("BE")
    Columns("P").Select
    Selection.Delete Shift:=xlToLeft

    'Deleting Header Row
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("N").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[1]=""NP"",""true"",""false"")"
    Range("M1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "N1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("U1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "T1").Select
    Selection.TextToColumns Destination:=Range("T1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "1"
    Range("T2").Activate
    Selection.FillDown

    
    Columns("T").Replace What:="Rescheduled", Replacement:=""
    Columns("T").Replace What:="Cancelled", Replacement:=""

    Columns("T").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
 
    'Deleting Extra Columns
    Columns("A:E").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Columns("B:C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Cut Destination:=Columns("R")
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L").Select
    Selection.Cut Destination:=Columns("R")
    
    'Moving Physician Name Column
    Columns("A").Select
    Selection.Cut Destination:=Columns("N")
    
    'Moving Appointment Date and Time Columns
    Columns("B:C").Select
    Selection.Cut Destination:=Columns("O:P")

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    'Moving Name Column
    Columns("E").Select
    Selection.Cut Destination:=Columns("A")
    
    'Moving Phone Column
    Columns("F").Select
    Selection.Cut Destination:=Columns("E")
    
    'Moving mrn Column
    Columns("D").Select
    Selection.Cut Destination:=Columns("K")

    'Moving dob Column
    Columns("H").Select
    Selection.Cut Destination:=Columns("D")

    'Moving Phone Column
    Columns("I").Select
    Selection.Cut Destination:=Columns("F")

    Columns("G").ClearContents

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    'Spliting Name Column to FirstName and LastName
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    
    'Triming LastName Column
    Columns("B").Select
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=TRIM(RC[-1])"
    Columns("C").Select
    Selection.FillDown
    Columns("C").Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("B").Select
    Selection.ClearContents

    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    'Splitting Lastname column to get middle name
    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft

    'Moving Middle Name Column
    Columns("D").Select
    Selection.Cut Destination:=Columns("B")

    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")
    
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    
    'Formatting Date Column
    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Columns("P").Select
    Selection.FillDown
    Columns("P").Copy
    Columns("P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("O").Select
    Selection.Delete Shift:=xlToLeft

    'Formatting Time Column
    Columns("Q").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""hh:mm AM/PM""))"
    Columns("Q").Select
    Selection.FillDown
    Columns("Q").Copy
    Columns("Q").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("P").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(1, 1), TrailingMinusNumbers:=True
    
    Columns("P").Select
    Selection.TextToColumns Destination:=Range("P1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(1, 1), TrailingMinusNumbers:=True

    'clean up phones
    Columns("E").Replace What:="-", Replacement:=" "
    Columns("E").Replace What:="(", Replacement:="+1 ("

    Columns("M").Select
    Selection.Cut Destination:=Columns("R")

    Columns("AR").Select
    Selection.Cut Destination:=Columns("S")

    Columns("S").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    
    Columns("S").Select
    Selection.Delete Shift:=xlToLeft

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    
    'adding data to duration column
    Range("Q2").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q2").Select
    Selection.FillDown

    Range("A1").Select
    Range(Selection, Selection.End(xlToRight)).Select
    Range(Selection, Selection.End(xlDown)).Select
    ActiveSheet.Columns("A:R").RemoveDuplicates Columns:=Array(1, 2, 3, 4, 5, 6, _
        7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18), Header:=xlYes
    
End Sub












