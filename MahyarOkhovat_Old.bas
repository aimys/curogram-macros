Attribute VB_Name = "Module1"
Sub Main()

    With ActiveWindow
        .SplitColumn = 0
        .SplitRow = 0
    End With
    ActiveWindow.DisplayGridlines = True

    'Deleting Header Rows
    Rows("1:8").Select
    Selection.Delete Shift:=xlUp

    'Resizing Columns
    Cells.Select
    Selection.ColumnWidth = 8

    'Unmerging Cells
    Cells.Select
    Selection.UnMerge

    'Deleting Extra Columns
    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C:K").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E:G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F:H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G:I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H:AF").Select
    Selection.Delete Shift:=xlToLeft

    'Filling Date to empty columns
    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Selection.FillDown

     
    Columns("A").Select
    Columns("A").Copy
    Columns("A").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Cells.Select
    With Selection.Font
        .Name = "Calibri"
        .Size = 9
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .TintAndShade = 0
        .ThemeFont = xlThemeFontMinor
    End With
    With Selection.Font
        .ColorIndex = xlAutomatic
        .TintAndShade = 0
    End With
    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    Cells.Select
    Cells.EntireColumn.AutoFit

   ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove


    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("G").Select
    Selection.Cut Destination:=Columns("A")
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 9), Array(5, 9), Array(6, 9)), _
        TrailingMinusNumbers:=True
    Columns("D").Select

    ' clean up phone cloumn
    Columns("C").Replace What:="-", Replacement:=""
    Columns("C").Replace What:="1", Replacement:=""
    Columns("C").Replace What:="2", Replacement:=""
    Columns("C").Replace What:="3", Replacement:=""
    Columns("C").Replace What:="4", Replacement:=""
    Columns("C").Replace What:="5", Replacement:=""
    Columns("C").Replace What:="6", Replacement:=""
    Columns("C").Replace What:="7", Replacement:=""
    Columns("C").Replace What:="8", Replacement:=""
    Columns("C").Replace What:="9", Replacement:=""
    Columns("C").Replace What:="0", Replacement:=""



    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[+1]="""","""",IF(RC[-1]="""",RC[-2],RC[-1]))"
    Columns("D").Select
    Selection.FillDown
    Columns("D").Copy
    Columns("D").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",SUBSTITUTE(RC[-2],RC[-1],""""))"
    Columns("D").Select
    Selection.FillDown
    Columns("D").Copy
    Columns("D").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D").Copy
    Columns("B").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Select
    Selection.Cut Destination:=Columns("O")
    Columns("E").Select
    Selection.Cut Destination:=Columns("P")
    Columns("H").Select
    Selection.Cut Destination:=Columns("D")
    Columns("F").Select
    Selection.Cut Destination:=Columns("K")
    Columns("I:J").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("E:F").Select


    Columns("E:E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Columns("E:E").Select
    Selection.FillDown
    Columns("E:E").Copy
    Columns("E:E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D:D").Select
    Selection.Delete Shift:=xlToLeft
    Columns("P:P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Columns("P:P").Select
    Selection.FillDown
    Columns("P:P").Copy
    Columns("P:P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("O:O").Select
    Selection.Delete Shift:=xlToLeft
    Columns("P:P").Select
    Selection.TextToColumns Destination:=Range("P1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="-", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    Range("R1").Select
    Columns("P:P").EntireColumn.AutoFit

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=TRIM(RC[-2])"
    Range("R1").Select
    Selection.AutoFill Destination:=Range("R1:S1"), Type:=xlFillDefault
    Range("R1:S1").Select
    Columns("R:R").Select
    Selection.FillDown
    Columns("S:S").Select
    Selection.FillDown
    Columns("R:S").Select
    Columns("R:S").Copy
    Columns("P:Q").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("R:S").Select
    Selection.ClearContents
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEN(RC[-2])=6,CONCATENATE(""0"",LEFT(RC[-2],4),"" "",RIGHT(RC[-2],2)),CONCATENATE(LEFT(RC[-2],5),"" "",RIGHT(RC[-2],2)))"
    Columns("R:R").Select
    Selection.FillDown
    Range("R1").Select

    Selection.AutoFill Destination:=Range("R1:S1"), Type:=xlFillDefault
    Range("R1:S1").Select
    Columns("S:S").Select
    Selection.FillDown
    Columns("R:S").Select
    Columns("R:S").Copy
    Columns("P:Q").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("R:S").Select
    Selection.Delete Shift:=xlToLeft
    Columns("P").EntireColumn.AutoFit


    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(TRIM(RC[-2])="""","""",(RC[-1]-RC[-2])*1440)"
    Columns("R:R").Select
    Selection.FillDown
    Columns("R:R").Copy
    Columns("R:R").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("Q:Q").Select
    Selection.Delete Shift:=xlToLeft


    'clean up phones
    Columns("E:F").Replace What:="-", Replacement:=" "
    Columns("E:F").Replace What:="(", Replacement:="+1 ("

    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=TRIM(TEXT(RC[1],""hh:mm AM/PM""))"
    Columns("P").Select
    Selection.FillDown
    Columns("P").Copy
    Columns("P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("Q").Select
    Selection.Delete Shift:=xlToLeft


    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("L2").Select

End Sub


