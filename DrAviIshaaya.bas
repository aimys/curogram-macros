Sub Main()

    ActiveWindow.DisplayGridlines = True
    
    Cells.Select
    Selection.Font.Bold = False
    With Selection.Font
        .Name = "ARIAL"
        .Size = 11
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .ColorIndex = 1
        .TintAndShade = 0
        .ThemeFont = xlThemeFontNone
    End With
    
    Columns("A:AZ").Select
    Selection.UnMerge
    
    Columns("B:D").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C:N").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D:S").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E:I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=R[1]C[-1]"
    Columns("E").Select
    Selection.FillDown
    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Columns("E").Select
    Columns("E").Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    
    Columns("C").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    
    Cells.EntireColumn.AutoFit
    Columns("A").Select
    Columns("A").Copy
    Columns("O").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Selection.Cut Destination:=Columns("O")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    Columns("C").Select
    Selection.Cut Destination:=Columns("K")
    Columns("F").Select
    Selection.Cut Destination:=Columns("C")
    Columns("D:E").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("C").Select
    Selection.Cut Destination:=Columns("D")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
        
    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 9), Array(2, 1)), TrailingMinusNumbers:=True
        
    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("A:C")
    
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft


    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]="""","""",TEXT(RC[-2],""hh:mm AM/PM""))"

    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "P1").Select
    Selection.FillDown

    Columns("P:Q").Copy
    Columns("P:Q").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("O").Select
    Selection.Delete Shift:=xlToLeft

    Range("G1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=0,"""",RC[-1])"

    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "G1").Select
    Selection.FillDown
    Columns("G").Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("G").Select
    Selection.ClearContents

    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Range("D1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "E1").Select
    Selection.FillDown
    Columns("E").Copy
    Columns("D").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft

    'clean up phones
    Columns("E:F").Replace What:="-", Replacement:=" "
    Columns("E:F").Replace What:="(", Replacement:="+1 ("

    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O2").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown

    Range("N1").Activate
    ActiveCell.FormulaR1C1 = "Ishaaya, Abraham"
    Range("O2").End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "N1").Select
    Selection.FillDown

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("L2").Select

End Sub
