Sub Main()

    If WorksheetExists("Sheet2") then
        Sheets("Sheet2").Select
        Range(Selection, Selection.End(xlDown)).Select
        Selection.Cut
        Sheets("Sheet1").Select
        Range("A1").Select
        Selection.End(xlDown).Select
        Range(ActiveCell.Offset(1, 0), ActiveCell.Offset(1, 0)).Select
        ActiveSheet.Paste
        Selection.End(xlDown).Select
        Range(ActiveCell.Offset(1, 0), ActiveCell.Offset(1, 0)).Select
    End If

    If WorksheetExists("Sheet3") then
        Sheets("Sheet3").Select
        Range(Selection, Selection.End(xlDown)).Select
        Selection.Cut
        Sheets("Sheet1").Select
        ActiveSheet.Paste
        Selection.End(xlDown).Select
        Range(ActiveCell.Offset(1, 0), ActiveCell.Offset(1, 0)).Select
    End If

    If WorksheetExists("Sheet4") then
        Sheets("Sheet4").Select
        Range(Selection, Selection.End(xlDown)).Select
        Selection.Cut
        Sheets("Sheet1").Select
        ActiveSheet.Paste
        Selection.End(xlDown).Select
        Range(ActiveCell.Offset(1, 0), ActiveCell.Offset(1, 0)).Select
    End If

    If WorksheetExists("Sheet5") then
        Sheets("Sheet5").Select
        Range(Selection, Selection.End(xlDown)).Select
        Selection.Cut
        Sheets("Sheet1").Select
        ActiveSheet.Paste
        Selection.End(xlDown).Select
        Range(ActiveCell.Offset(1, 0), ActiveCell.Offset(1, 0)).Select
    End If

    If WorksheetExists("Sheet6") then
        Sheets("Sheet6").Select
        Range(Selection, Selection.End(xlDown)).Select
        Selection.Cut
        Sheets("Sheet1").Select
        ActiveSheet.Paste
        Selection.End(xlDown).Select
        Range(ActiveCell.Offset(1, 0), ActiveCell.Offset(1, 0)).Select
    End If

    If WorksheetExists("Sheet7") then
        Sheets("Sheet7").Select
        Range(Selection, Selection.End(xlDown)).Select
        Selection.Cut
        Sheets("Sheet1").Select
        ActiveSheet.Paste
    End If

    Columns("C").Replace What:="Chart #", Replacement:=""

    Cells.Select
    Selection.UnMerge

    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[-1],3)=""App"",RIGHT(RC[-1],10),"""")"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks:=False, Transpose:=False
    Application.CutCopyMode = False

    Rows("1:2").Select
    Selection.Delete Shift:=xlUp

    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B1").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Selection.SpecialCells(xlCellTypeBlanks).Select
    Range("B2").Activate
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("B3").Activate
    Selection.FillDown
    Columns("B").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    Columns("I").Select
    Selection.Delete Shift:=xlToLeft

    Columns("B").Select
    With Selection.Font
        .Size = 8
        .Strikethrough = False
        .Superscript = False
        .Subscript = False
        .OutlineFont = False
        .Shadow = False
        .Underline = xlUnderlineStyleNone
        .TintAndShade = 0
        .ThemeFont = xlThemeFontNone
    End With
    Selection.Font.Bold = False
    
    Columns("J").ColumnWidth = 9
    Columns("I").ColumnWidth = 9
    
    Columns("D").Select
    Selection.Cut Destination:=Columns("K")
    Columns("B").Select
    Selection.Cut Destination:=Columns("O")
    Columns("A").Select
    Selection.Cut Destination:=Columns("P")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("H").Select
    Selection.Cut Destination:=Columns("N")
    Columns("G").Select
    Selection.Cut Destination:=Columns("R")
    Columns("E").Select
    Selection.ClearContents
    
    Columns("F").Select
    Selection.Cut Destination:=Columns("E")

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    Selection.Cut Destination:=Columns("C")
    
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=""NEW PATIENT"",""true"",""false"")"
    Range("R1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "S1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks:=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("R").Select
    Selection.Cut Destination:=Columns("M")

    Columns("R").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Replace What:="(", Replacement:="+1 ("
    Columns("E").Replace What:="-", Replacement:=" "

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("R1").End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "Q1").Select
    Selection.FillDown

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub

Function WorksheetExists(shtName As String, Optional wb As Workbook) As Boolean
    Dim sht As Worksheet

     If wb Is Nothing Then Set wb = ThisWorkbook
     On Error Resume Next
     Set sht = wb.Sheets(shtName)
     On Error GoTo 0
     WorksheetExists = Not sht Is Nothing
 End Function