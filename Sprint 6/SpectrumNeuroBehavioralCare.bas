Sub Main()

    Cells.Select
    Selection.ClearFormats
    ActiveSheet.ListObjects("Table").TableStyle = ""
    With ActiveWindow
        .SplitColumn = 0
        .SplitRow = 0
    End With

    ActiveWindow.DisplayGridlines = True

    Columns("B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E:F").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Select
    Selection.NumberFormat = "m/d/yyyy"
    Columns("D").Select
    Selection.NumberFormat = "m/d/yyyy"

    Columns("H:I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K:AC").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("I").Select
    Selection.Cut Destination:=Columns("V")
    
    Columns("E:F").Select
    Selection.Cut Destination:=Columns("R:S")
    Columns("G").Select
    Selection.Cut Destination:=Columns("M")
    Columns("J").Select
    Selection.Cut Destination:=Columns("O")
    Columns("H").Select
    Selection.Cut Destination:=Columns("N")
    Columns("A").Select
    Selection.Cut Destination:=Columns("K")

    Columns("K").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("B").Select
    Selection.Cut Destination:=Columns("K")
    Columns("C").Select
    Selection.Cut Destination:=Columns("M")
    Columns("D").Select
    Selection.Cut Destination:=Columns("N")
    
    Columns("A:J").Select
    Selection.Delete Shift:=xlToLeft
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("D").Replace What:="/", Replacement:="."
    Columns("R").Replace What:="/", Replacement:="."
    
    Columns("S").Replace What:=" ", Replacement:=""

    Range("T1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("S1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "T1").Select
    Selection.FillDown

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "FALSE"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

End Sub
