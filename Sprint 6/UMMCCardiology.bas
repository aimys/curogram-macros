Sub Main()

    Sheets.Add After:=Sheets(Sheets.Count)
    Sheets("Appointment Report").Select
    Cells.Select
    Selection.Copy
    Sheets("Sheet1").Select
    Selection.PasteSpecial Paste:=xlPasteValuesAndNumberFormats, Operation:= _
        xlNone, SkipBlanks:=False, Transpose:=False
    Cells.Select
    Cells.EntireColumn.AutoFit
    Range("F13").Select
    Sheets("Appointment Report").Select
    Application.CutCopyMode = False
    ActiveWindow.SelectedSheets.Delete
    Sheets("CPT Details").Select
    ActiveWindow.SelectedSheets.Delete

    Columns("A:B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E:G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Cut Destination:=Columns("O")
    Columns("B:C").Select
    Selection.Cut Destination:=Columns("R:S")
    Columns("D").Select
    Selection.Cut Destination:=Columns("V")
    Columns("F").Select
    Selection.Cut Destination:=Columns("A")
    Columns("E").Select
    Selection.Cut Destination:=Columns("C")
    Columns("K").Select
    Selection.Cut Destination:=Columns("D")
    Columns("G").Select
    Selection.Cut Destination:=Columns("K")
    Columns("H:I").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("J").Select
    Selection.Cut Destination:=Columns("T")
    Columns("N").Select
    Selection.Cut Destination:=Columns("P")
    Columns("L").Select
    Selection.Cut Destination:=Columns("N")
    Columns("M").Select
    Selection.Cut Destination:=Columns("I")

    Range("U2").Select
    ActiveCell.FormulaR1C1 = "=(RC[-1]-RC[-2])*1440"
    Range("T2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U2").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("T2").Select
    Range(Selection, Selection.End(xlDown)).Select
    Selection.ClearContents

    Range("U2").Select
    Range(Selection, Selection.End(xlDown)).Select
    Selection.Cut Destination:=Range("T2")

    Range("U2").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-7]=""New Patient"",""true"",""false"")"
    Range("T2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U2").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D").Replace What:="/", Replacement:="."
    Columns("R").Replace What:="/", Replacement:="."
    Columns("S").Replace What:=" ", Replacement:=""

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"


End Sub