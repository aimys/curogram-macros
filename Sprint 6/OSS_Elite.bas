Sub Main()

    Rows("1:6").Select
    Selection.Delete Shift:=xlUp

    Columns("E:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K:M").Select
    Selection.Delete Shift:=xlToLeft
    Columns("O:P").Select
    Selection.Delete Shift:=xlToLeft
    Columns("P:AE").Select
    Selection.Delete Shift:=xlToLeft

    Columns("F").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("A:C").Select
    Selection.Cut Destination:=Columns("R:T")
    Columns("F").Select
    Selection.Cut Destination:=Columns("A")
    Columns("D").Select
    Selection.Cut Destination:=Columns("V")
    Columns("E").Select
    Selection.Cut Destination:=Columns("D")
    Columns("G").Select
    Selection.Cut Destination:=Columns("E")
    Columns("H:K").Select
    Selection.Cut Destination:=Columns("F:I")
    Columns("L").Select
    Selection.Cut Destination:=Columns("J")
    Columns("M").Select
    Selection.Cut Destination:=Columns("P")
    Columns("O").Select
    Selection.Cut Destination:=Columns("M")
    Columns("P").Select
    Selection.Cut Destination:=Columns("O")

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=(RC[-1]-RC[-2])*1440"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("T").Select
    Selection.ClearContents
    Columns("U").Select
    Selection.Cut Destination:=Columns("T")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("B").Select
    Selection.Cut Destination:=Columns("C")
    
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Select
    Selection.Cut Destination:=Columns("K")

    Columns("E:J").Select
    Selection.Cut Destination:=Columns("D:I")

    Columns("D").Replace What:="/", Replacement:="."
    Columns("R").Replace What:="/", Replacement:="."

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "false"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown

    'clean up phones
    Columns("E:H").Replace What:="-", Replacement:=" "
    Columns("E:H").Replace What:="(", Replacement:="+1 ("
    Columns("E:H").Replace What:=")", Replacement:=") "

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("H").Select
    Selection.ClearContents

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

    Columns("O").Select
    Selection.AutoFilter
    Columns("O").AutoFilter Field:=1, Criteria1:=Array("Martha Paterson,", "Personal Training,", "="), Operator:=xlFilterValues
    Range("O2").Select
    Range(Selection, Selection.End(xlDown)).Select
    Selection.ClearContents
    Columns("O").AutoFilter Field:=1

    Columns("O").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete


End Sub