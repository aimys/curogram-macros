Sub Main()

    Cells.Select
    Selection.UnMerge
    
    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    
    ActiveWindow.DisplayGridlines = True

    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("B2").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[-1],7)=""Patient"",R[1]C[-1],"""")"
    Range("A2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B2").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("B2").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[-1],7)=""Patient"",R[-1]C[-1],"""")"
    Range("A2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B2").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Rows("1:11").Select
    Selection.Delete Shift:=xlUp

    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("B3").Activate
    Selection.FillDown
    Columns("B").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("C").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("D2").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]=R[-1]C[-1],"""",RC[-2])"
    Range("C2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "D2").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("D").Select
    Selection.TextToColumns Destination:=Range("D1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("D").Select        
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("D5").Activate
    Selection.FillDown
    Columns("D").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.Delete Shift:=xlToLeft

    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True
    
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("E").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("A").Select
    Selection.Cut Destination:=Columns("F")

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Select
    Selection.TextToColumns Destination:=Range("E1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 9), Array(18, 1)), TrailingMinusNumbers:=True
    
    Columns("E").Select
    Selection.TextToColumns Destination:=Range("E1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1), Array(5, 1), Array(6, 1), _
        Array(7, 1)), TrailingMinusNumbers:=True

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 1), Array(8, 9)), TrailingMinusNumbers:=True

    Columns("G").Select
    Selection.TextToColumns Destination:=Range("G1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 9), Array(6, 1)), TrailingMinusNumbers:=True

    Columns("H").Select
    Selection.TextToColumns Destination:=Range("H1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 9), Array(6, 1)), TrailingMinusNumbers:=True

    Columns("C").Replace What:=" Monday", Replacement:=""
    Columns("C").Replace What:=" Tuesday", Replacement:=""
    Columns("C").Replace What:=" Wednesday", Replacement:=""
    Columns("C").Replace What:=" Thursday", Replacement:=""
    Columns("C").Replace What:=" Friday", Replacement:=""
    Columns("C").Replace What:=" Saturday", Replacement:=""
    Columns("C").Replace What:=" Sunday", Replacement:=""

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-2],"","",RC[-1])"
    Range("E1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B:C").Select
    Selection.Delete Shift:=xlToLeft

    Range("L1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]<>"""",RC[-1],IF(RC[-2]<>"""",RC[-2],IF(RC[-3]<>"""",RC[-3],"""")))"
    Range("H1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 4), "L1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("I").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(LEFT(RC[1],5)="" Home"",LEFT(RC[1],5)="" E-Ma"",LEFT(RC[1],5)="" Mobi"",LEFT(RC[1],5)="" E-Ma"",LEFT(RC[1],5)="" Busi"",LEFT(RC[1],5)="" Cell""),RC[1],"""")"
    Range("H1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "I1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("J").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("J").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[1],5)="" E-Ma"",RC[1],"""")"
    Range("H1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "J1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("K:L").Select
    Selection.Delete Shift:=xlToLeft

    Columns("H").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("L").Select
    Selection.Cut Destination:=Columns("H")

    Columns("H").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[1],5)="" E-Ma"","""",RC[1])"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "H1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("I").Select
    Selection.Delete Shift:=xlToLeft

    Columns("J").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[1],5)="" E-Ma"","""",RC[1])"
    Range("I1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "J1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("K").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[1],5)="" E-Ma"",RC[1],"""")"
    Range("I1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "K1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("L").Select
    Selection.Delete Shift:=xlToLeft

    Range("M1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-2],RC[-1])"
    Range("I1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 4), "M1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("K:L").Select
    Selection.Delete Shift:=xlToLeft

    Columns("H").Select
    Selection.Cut Destination:=Columns("M")
    Columns("A").Select
    Selection.Cut Destination:=Columns("P")
    Columns("C").Select
    Selection.Cut Destination:=Columns("O")
    Columns("B").Select
    Selection.Cut Destination:=Columns("N")
    Columns("D").Select
    Selection.Cut Destination:=Columns("A")
    Columns("E").Select
    Selection.Cut Destination:=Columns("C")
    Columns("F").Select
    Selection.Cut Destination:=Columns("D")
    Columns("I:J").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("K").Select
    Selection.Cut Destination:=Columns("I")
    Columns("G").Select
    Selection.Cut Destination:=Columns("K")

    Columns("E:F").Replace What:=" Cell Phone: ", Replacement:=""
    Columns("E:F").Replace What:=" Home: ", Replacement:=""
    Columns("E:F").Replace What:=" Business: ", Replacement:=""
    Columns("E:F").Replace What:=" Cellular: ", Replacement:=""
    Columns("E:F").Replace What:=" Mobile: ", Replacement:=""
    Columns("E:F").Replace What:=" Clinic: ", Replacement:=""
    Columns("I").Replace What:=" E-Mail: ", Replacement:=""

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-5]="" New Patient"",""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("P").Replace What:=" ", Replacement:=""

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")
    
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"


End Sub