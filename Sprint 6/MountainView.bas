Sub Main()

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Select
    Selection.Cut Destination:=Columns("R")
    
    Columns("R").Select
    Selection.TextToColumns Destination:=Range("R1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    
    Columns("S").Select
    Selection.TextToColumns Destination:=Range("S1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 9), Array(1, 1), Array(9, 9), Array(12, 1), Array(20, 9)), _
        TrailingMinusNumbers:=True

    Columns("G").Select
    Selection.Cut Destination:=Columns("V")
    Columns("B").Select
    Selection.Cut Destination:=Columns("K")
    Columns("E").Select
    Selection.Cut Destination:=Columns("N")
    Columns("F").Select
    Selection.Cut Destination:=Columns("O")
    Columns("C").Select
    Selection.Cut Destination:=Columns("M")

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=(RC[-1]-RC[-2])*1440"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("T").Select
    Selection.ClearContents
    
    Columns("U").Select
    Selection.Cut Destination:=Columns("T")

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "FALSE"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 9)), TrailingMinusNumbers:= _
        True
    
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""",RC[-2],RC[-1])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.ClearContents
    
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")
    
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=RC[-2],"""",RC[-2])"
    Range("C1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.ClearContents

    Columns("D").Select
    Selection.Cut Destination:=Columns("B")

    Columns("S").Replace What:=" ", Replacement:=""
    Columns("R").Replace What:="/", Replacement:="."
    
    'Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"
    
End Sub
