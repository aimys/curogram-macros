Sub Main()

    Columns("A").Select
    Selection.Cut Destination:=Columns("AX")

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 1), Array(18, 9), Array(32, 9), Array(59, 9), Array(84, 9), _
        Array(120, 9), Array(145, 9)), TrailingMinusNumbers:=True

    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("H").Select
    Selection.Cut Destination:=Columns("N")
    Columns("G").Select
    Selection.Cut Destination:=Columns("P")
    Columns("B").Select
    Selection.Cut Destination:=Columns("O")
    Columns("A").Select
    Selection.Cut Destination:=Columns("L")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""",RC[-2],RC[-1])"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -2), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")
    Columns("D").Select
    Selection.ClearContents

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=RC[-2],"""",RC[-2])"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -2), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D").Select
    Selection.Cut Destination:=Columns("B")
    Columns("D").Select
    Selection.ClearContents

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-1],"" "",RC[1])"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -2), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")
    Columns("D:E").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Select
    Selection.TextToColumns Destination:=Range("E1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 9), Array(3, 1)), TrailingMinusNumbers:=True
    Columns("F").Select
    Selection.TextToColumns Destination:=Range("F1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 9), Array(3, 1)), TrailingMinusNumbers:=True

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O2").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown

    'clean up phones
    Columns("E:F").Replace What:="-", Replacement:=""
    Columns("E:F").Replace What:="(", Replacement:=""
    Columns("E:F").Replace What:=")", Replacement:=""
    Columns("E:F").Replace What:=" ", Replacement:=""

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    'Formatting Phone1 Columns
    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],""-"",""""),""+1 (###) ### ####""))"
    Columns("E").Select
    Selection.FillDown
    Selection.Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("E").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Formatting Phone2 Columns
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],""-"",""""),""+1 (###) ### ####""))"
    Columns("F").Select
    Selection.FillDown
    Selection.Copy
    Columns("G").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("F").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft

    Columns("L").Select
    Selection.Cut Destination:=Columns("O")

    Columns("AW").Select
    Selection.Cut Destination:=Columns("L")

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"

End Sub
