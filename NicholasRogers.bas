Sub Main()
 'Copying Data to new Sheet
    Cells.Select
    Selection.ClearFormats
    ActiveWindow.DisplayGridlines = True

    'Deleting Excess Rows And Columns
    Rows("1:3").Select
    Selection.Delete Shift:=xlUp
    Rows("2").Select
    Selection.Delete Shift:=xlUp
    Columns("B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J").Select
    Selection.Delete Shift:=xlToLeft

    'Filling Appointment Date Column
    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Selection.FillDown
    Columns("A").Select
    Columns("A").Copy
    Columns("A").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    'Deleting Empty Rows
    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    Range("A1").Select
    Selection.End(xlDown).Select
    Selection.EntireRow.Delete

    'Deleting Excess Data
    Columns("J").Select
    Selection.Delete Shift:=xlToLeft

    'Moving Service,Appointment Date,Time,Duration Columns
    Columns("I").Select
    Selection.Cut Destination:=Columns("L")
    Columns("A:C").Select
    Selection.Cut Destination:=Columns("N:P")

    'Replacing Name Column
    Columns("E").Select
    Selection.Cut Destination:=Columns("A")

    'Splitting Name Column to firstname and lastname
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("A").Select
    Selection.Cut Destination:=Columns("C")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    Columns("C").Select
    Selection.Cut Destination:=Columns("B")

    'adding middlename column
    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    'moving dob,phone,physicianName and mrn columns
    Columns("G").Select
    Selection.Cut Destination:=Columns("D")
    Columns("E").Select
    Selection.Cut Destination:=Columns("K")
    Columns("I").Select
    Selection.Cut Destination:=Columns("N")
    Columns("H").Select
    Selection.Cut Destination:=Columns("E")
    
    'splitting duration Date Column
    Columns("Q").Select
    Selection.TextToColumns Destination:=Range("Q1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    Columns("R").Select
    Selection.Delete Shift:=xlToLeft
    
    'splitting appointment Date Column
    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :=",", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    Selection.Delete Shift:=xlToLeft
    
    'Formatting dob column
    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Columns("E").Select
    Selection.FillDown
    Columns("E").Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    
    'Formatting appointment date column
    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]= """","""",TEXT(TRIM(RC[-1]),""mm.dd.yyyy""))"
    Columns("P").Select
    Selection.FillDown
    Columns("P").Copy
    Columns("P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("O").Select
    Selection.Delete Shift:=xlToLeft
    
    'Formatting phone1 column
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-1]="""","""",TEXT(SUBSTITUTE(RC[-1],""-"",""""),""+1 (###) ### ####""))"
    Columns("F").Select
    Selection.FillDown
    Columns("F").Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    
    'Formatting appointment time column
    Columns("Q").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=TRIM(RC[-1])"
    Columns("Q").Select
    Selection.FillDown
    Columns("Q").Copy
    Columns("Q").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("P").Select
    Selection.Delete Shift:=xlToLeft

    'Deleting Header Row
    Rows("1").Select
    Selection.Delete Shift:=xlUp

   ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"

End Sub
