Sub Main()
    Rows("1:2").Select
    Selection.Delete Shift:=xlUp

    Columns("F:H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H:I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K").Select
    Selection.Delete Shift:=xlToLeft

    Columns("H").Select
    Selection.Cut Destination:=Columns("O")

    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "P1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-2],""hh:mm AM/PM"")"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("O").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("J").Select
    Selection.Cut Destination:=Columns("L")
    Columns("I").Select
    Selection.Cut Destination:=Columns("M")
    Columns("G").Select
    Selection.Cut Destination:=Columns("N")
    Columns("E").Select
    Selection.Cut Destination:=Columns("I")
    Columns("F").Select
    Selection.Cut Destination:=Columns("J")
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("E:G")
    Columns("J").Select
    Selection.Cut Destination:=Columns("D")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="-", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.Cut Destination:=Columns("K")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, OtherChar _
        :="-", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    
    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, OtherChar _
        :="-", FieldInfo:=Array(Array(1, 9), Array(2, 1), Array(3, 1)), _
        TrailingMinusNumbers:=True
        
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-4]=""New Patient"",""true"",""false"")"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D").Replace What:="/", Replacement:="."

    Columns("I").Replace What:="Sarah", Replacement:=""
    Columns("I").Replace What:="NONE@NONE.COM", Replacement:=""
    Columns("I").Replace What:="NONE@msn.com", Replacement:=""
    Columns("I").Replace What:="NONE@gmail.com", Replacement:=""
    Columns("I").Replace What:="NONE@yahoo.com", Replacement:=""

    Columns("D").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("Q").Select
    Selection.Cut Destination:=Columns("R")

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub
