Sub Main()

    With ActiveWindow
        .SplitColumn = 0
        .SplitRow = 0
    End With

    Rows("1:6").Select
    Selection.Delete Shift:=xlUp

    Columns("J").Select
    Selection.Cut Destination:=Columns("BB")
    
    Columns("J").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",IF(LEFT(RC[-1],5)=""TOTAL"","""",RC[-1]))"
    Columns("C").Select
    Selection.FillDown
    
    Columns("C").Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
        
    Columns("A:B").Select
    Selection.Delete Shift:=xlToLeft
    Range("B1").Select
    Selection.End(xlDown).Select
    Range("A2").Select
    Selection.FillDown
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo:= _
        Array(1, 1), TrailingMinusNumbers:=True
        
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("A2000").Activate
    Selection.FillDown
    Columns("A").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    
    Columns("M").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("O:Q")
    Columns("A").Select
    Selection.Cut Destination:=Columns("N")
    Columns("E").Select
    Selection.Cut Destination:=Columns("A")

    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("G").Select
    Selection.ClearContents
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[1]=""New Patient"",""true"",""false"")"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "G1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("H").Select
    Selection.Cut Destination:=Columns("S")
    Columns("G").Select
    Selection.Cut Destination:=Columns("R")

    Columns("G:H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("K").Select
    Selection.Cut Destination:=Columns("I")
    Columns("J").Select
    Selection.Cut Destination:=Columns("K")

    Columns("L").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Range("D1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-2] = """","""",CONCATENATE(TRIM(RC[-2]),"" "",TRIM(RC[-1])))"
    Columns("D").Select
    Selection.FillDown
    Columns("D").Copy
    Columns("B").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.ClearContents
    Columns("D").Select
    Selection.ClearContents

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
        
    Columns("D:E").Select
    Selection.ClearContents
        
    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    Columns("C").Select
    Selection.Cut Destination:=Columns("B")
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")
    Columns("F").Select
    Selection.Cut Destination:=Columns("D")
    Columns("G:K").Select
    Selection.Cut Destination:=Columns("E:I")

    Columns("I").Select
    Selection.Cut Destination:=Columns("G")
    Columns("F").Select
    Selection.Cut Destination:=Columns("I")
    Columns("G").Select
    Selection.Cut Destination:=Columns("F")
    
    Cells.Replace What:="", Replacement:="", LookAt:=xlPart, _
    SearchOrder:=xlByColumns, MatchCase:=False, SearchFormat:=False, _
    ReplaceFormat:=False

    'clean up phones
    Columns("E:G").Replace What:="-", Replacement:=" "
    Columns("E:G").Replace What:="(", Replacement:="+1 ("
    Columns("E:G").Replace What:=")", Replacement:=") "

    Columns("D").Replace What:="/", Replacement:="."
    Columns("P").Replace What:="/", Replacement:="."

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("N").Select
    Selection.Cut Destination:=Columns("K")

    Columns("N").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TRIM(TEXT(RC[1],""hh:mm AM/PM"")))"
    Columns("P").Select
    Selection.FillDown
    Columns("P").Copy
    Columns("P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("Q").Select
    Selection.Delete Shift:=xlToLeft

    Columns("S").Select
    Selection.Cut Destination:=Columns("M")

    Columns("AY").Select
    Selection.Cut Destination:=Columns("G")

    'clean up phones
    Columns("G").Replace What:="-", Replacement:=" "
    Columns("G").Replace What:="(", Replacement:="+1 ("
    Columns("G").Replace What:=")", Replacement:=") "

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

Columns("R").Select
    Selection.TextToColumns Destination:=Range("R1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
        
    
End Sub







