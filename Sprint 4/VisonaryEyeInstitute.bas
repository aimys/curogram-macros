Sub Main()

    With ActiveWindow
        .SplitColumn = 0
        .SplitRow = 0
    End With
    
    Rows("1:5").Select
    Selection.Delete Shift:=xlUp

    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",IF(LEFT(RC[-1],5)=""TOTAL"","""",RC[-1]))"
    Columns("B").Select
    Selection.FillDown
    
    Columns("B").Copy
    Columns("B").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo:= _
        Array(1, 1), TrailingMinusNumbers:=True

    Columns("A").Select    
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("A3").Activate
    Selection.FillDown
    Columns("A").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Rows("1").Select
    Selection.Delete Shift:=xlUp
    
    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",IF(LEFT(RC[-1],5)=""TOTAL"","""",RC[-1]))"
    Columns("C").Select
    Selection.FillDown
    
    Columns("C").Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
        
    Columns("B").Select
    Selection.Delete Shift:=xlToLeft

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo:= _
        Array(1, 1), TrailingMinusNumbers:=True

    Columns("B").Select    
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("B3").Activate
    Selection.FillDown
    Columns("B").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    
    Columns("C").Select
    Cells.Replace What:="Block", Replacement:="", LookAt:=xlWhole, _
        SearchOrder:=xlByRows, MatchCase:=False, SearchFormat:=False, _
        ReplaceFormat:=False

    Columns("A").Select
    Selection.Cut Destination:=Columns("AY")

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Columns("H").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("M").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("O:Q")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Z")
    Columns("A").Select
    Selection.Cut Destination:=Columns("N")
    Columns("E").Select
    Selection.Cut Destination:=Columns("A")

    Columns("G").Select
    Selection.ClearContents
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[1]=""New Patient Consult"",""true"",""false"")"
    Range("H1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "G1").Select
    Selection.FillDown
    Selection.Copy
    Columns("R").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("H").Select
    Selection.Cut Destination:=Columns("AM")
    
    Columns("G:H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("K").Select
    Selection.Cut Destination:=Columns("I")
    Columns("J").Select
    Selection.Cut Destination:=Columns("K")
    Columns("L").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("M").Select

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    

    Range("D1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-2] = """","""",CONCATENATE(TRIM(RC[-2]),"" "",TRIM(RC[-1])))"
    Columns("D").Select
    Selection.FillDown
    Columns("D").Copy
    Columns("B").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("C").Select
    Selection.ClearContents
    Columns("D").Select
    Selection.ClearContents
    
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
        
    Columns("F:G").Select
    Selection.Delete Shift:=xlToLeft
        
    Columns("D:E").Select
    Selection.ClearContents
        
    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    Columns("C").Select
    Selection.Cut Destination:=Columns("B")
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")
    Columns("F").Select
    Selection.Cut Destination:=Columns("D")
    Columns("G:K").Select
    Selection.Cut Destination:=Columns("E:I")
    
    Cells.Replace What:="", Replacement:="", LookAt:=xlPart, _
        SearchOrder:=xlByColumns, MatchCase:=False, SearchFormat:=False, _
        ReplaceFormat:=False

    
    'clean up phones
    Columns("E:G").Replace What:="-", Replacement:=" "
    Columns("E:G").Replace What:="(", Replacement:="+1 ("
    Columns("E:G").Replace What:=")", Replacement:=") "

    Columns("D").Replace What:="/", Replacement:="."
    Columns("P").Replace What:="/", Replacement:="."
    
    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TRIM(TEXT(RC[1],""hh:mmAM/PM"")))"
    Columns("P").Select
    Selection.FillDown
    Columns("P").Copy
    Columns("P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("Q").Select
    Selection.Delete Shift:=xlToLeft

    Columns("N").Select
    Selection.Cut Destination:=Columns("K")
    Columns("N").Select
    Selection.Delete Shift:=xlToLeft

    Columns("AM").Select
    Selection.Cut Destination:=Columns("M")

    Columns("F").Select
    Selection.TextToColumns Destination:=Range("F1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 1), Array(18, 9)), TrailingMinusNumbers:=True

    Columns("P").Select
    Selection.NumberFormat = "hh:mmAM/PM"

    Columns("Z").Select
    Selection.Cut Destination:=Columns("K")

    Columns("AW").Select
    Selection.Cut Destination:=Columns("L")

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")

    Columns("R").Select
    Selection.TextToColumns Destination:=Range("R1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("R").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub