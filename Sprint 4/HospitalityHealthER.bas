Sub Main()

    Columns("H").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("K").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I:J").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("H").Select
    Selection.Cut Destination:=Columns("M")
    Columns("G").Select
    Selection.Cut Destination:=Columns("K")

    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 16), "Q1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    Columns("P").Replace What:=" ", Replacement:=""

    Range("L1").Select
    ActiveCell.FormulaR1C1 = "EMERGENCY ROOM"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 11), "L1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub

