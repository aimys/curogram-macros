Sub Main()

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("A:B").Select
    Selection.ClearFormats

    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[1]="""",RC[-1],"""")"
    Range("B1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "C1").Select
    Selection.FillDown
    Columns("C").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("C3").Activate
    Selection.FillDown
    Columns("C").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[2]="""",TEXT(RC[-2],""mm.dd.yyyy""),"""")"
    Range("B1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "C1").Select
    Selection.FillDown
    Columns("C").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("C3").Activate
    Selection.FillDown
    Columns("C").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("H").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("H").Select
    Selection.Delete Shift:=xlToLeft
        
    Columns("D").Replace What:="Monday ", Replacement:=""
    Columns("D").Replace What:="Tuesday ", Replacement:=""
    Columns("D").Replace What:="Wednesday ", Replacement:=""
    Columns("D").Replace What:="Thursday ", Replacement:=""
    Columns("D").Replace What:="Friday ", Replacement:=""
    Columns("D").Replace What:="Saturday ", Replacement:=""
    Columns("D").Replace What:="Sunday ", Replacement:=""

    Columns("H").Select
    Selection.TextToColumns Destination:=Range("H1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 1), Array(14, 1)), TrailingMinusNumbers:=True
    
    Columns("I").Select
    Selection.TextToColumns Destination:=Range("I1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="*", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    Columns("J").Select
    Selection.TextToColumns Destination:=Range("J1"), DataType:=xlFixedWidth, _
        OtherChar:="*", FieldInfo:=Array(Array(0, 1), Array(15, 1)), _
        TrailingMinusNumbers:=True
    Columns("J").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("I").Select
    Selection.TextToColumns Destination:=Range("I1"), DataType:=xlFixedWidth, _
        OtherChar:="*", FieldInfo:=Array(Array(0, 1), Array(14, 1)), _
        TrailingMinusNumbers:=True

    Columns("H:L").Replace What:="x ", Replacement:=""
    Columns("H:L").Replace What:="x", Replacement:=""

    Columns("K").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""",RC[2],RC[-1])"
    Range("H1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "K1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("J").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L").Select
    Selection.Delete Shift:=xlToLeft

    Columns("J").Replace What:="0", Replacement:="", LookAt:=xlWhole

    Columns("G").Select
    Selection.Cut Destination:=Columns("L")
    
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("K").Select
    Selection.TextToColumns Destination:=Range("K1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 1), Array(2, 9)), TrailingMinusNumbers:=True

    Columns("K").Select
    Selection.TextToColumns Destination:=Range("K1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("L").Select
    Selection.TextToColumns Destination:=Range("L1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 9), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("C").Select
    Selection.Cut Destination:=Columns("O")
    Columns("A:B").Select
    Selection.Cut Destination:=Columns("P:Q")
    Columns("L").Select
    Selection.Cut Destination:=Columns("A")
    Columns("M").Select
    Selection.Cut Destination:=Columns("B")
    Columns("K").Select
    Selection.Cut Destination:=Columns("C")
    Columns("D").Select
    Selection.Cut Destination:=Columns("N")
    Columns("E").Select
    Selection.Cut Destination:=Columns("M")
    Columns("F").Select
    Selection.Cut Destination:=Columns("K")
    Columns("G:J").Select
    Selection.Cut Destination:=Columns("E:H")

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-5]=""NP 144 New Pt"",""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, OtherChar _
        :="(", FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    Columns("Q").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""hh:mmAM/PM"")"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("P").Select
    Selection.Delete Shift:=xlToLeft

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")


    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    
End Sub
