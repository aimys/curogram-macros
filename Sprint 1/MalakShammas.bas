Sub Main()

    Columns("N:AD").Select
    Selection.Delete Shift:=xlToLeft

    Columns("M").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=""NP PHYSICAL"",""true"",""false"")"
    Range("L1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "M1").Select
    Selection.FillDown
    Selection.Copy
    Columns("X").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("L").Select
    Selection.Cut Destination:=Columns("AZ")

    Columns("M").Select
    Selection.Delete Shift:=xlToLeft

    Columns("K:L").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G:I").Select
    Selection.Delete Shift:=xlToLeft
    
    ' Rows("1").Select
    ' Selection.Delete Shift:=xlUp
    
    Columns("A:B").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("H").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("D").Select
    Selection.Cut Destination:=Columns("K")
    Columns("G").Select
    Selection.Cut Destination:=Columns("D")
    Columns("F").Select
    Selection.Cut Destination:=Columns("N")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=True, Other:=False, FieldInfo:= _
        Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
        
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[1]="""","""",IF(RC[-1]="""",RC[-2],RC[-1]))"
    Range("E1").End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "D1").Select
    Selection.FillDown
    Columns("D").Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("D").Select
    Selection.ClearContents
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[1]="""","""",IF(RC[-1]=RC[-2],"""",RC[-2]))"
    Range("E1").End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "D1").Select
    Selection.FillDown
    Columns("D").Copy
    Columns("B").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."
    Columns("E").Replace What:="-", Replacement:=""

    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(""+1 ("",LEFT(RC[-1],3),"") "",RIGHT(RC[-1],7))"
    Range("D1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "F1").Select
    Selection.FillDown
    Columns("F").Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("F").Select
    Selection.ClearContents
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(LEFT(RC[-1],12),"" "",RIGHT(RC[-1],4))"
    Range("D1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "F1").Select
    Selection.FillDown
    Columns("F").Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("F").Select
    Selection.ClearContents

    Columns("B").Replace What:="0", Replacement:=""

    Columns("E").Replace What:="+1 ()   () ", Replacement:=""

    Columns("AT").Select
    Selection.Cut Destination:=Columns("M")

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

     Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub

