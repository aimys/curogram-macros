Sub Main()
    Rows("1:18").Select
    Selection.Delete Shift:=xlUp

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft

    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=TRIM(RC[-1])"
    Columns("C").Select
    Selection.FillDown
    Columns("C").Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("D").Select
    Selection.Cut Destination:=Columns("B")
    Columns("B").Select

    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")

    Columns("F").Select
    Selection.Cut Destination:=Columns("O")
    Columns("O").Select
    Selection.Copy
    Range("P1").Select
    ActiveSheet.Paste
    Application.CutCopyMode = False
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Columns("P").Select
    Selection.FillDown
    Columns("P").Copy
    Columns("P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("O").Select
    Selection.Delete Shift:=xlToLeft
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",IF(IF(CELL(""contents"",RC[-1])/INT(CELL(""contents"",RC[-1]))=1,TRUE,FALSE),"""",TEXT(RC[-1],""hh:mm AM/PM"")))"
    Columns("Q").Select
    Selection.FillDown
    Columns("Q").Copy
    Columns("Q").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("P").Select
    Selection.Delete Shift:=xlToLeft

    Columns("I").Select
    Selection.Cut Destination:=Columns("R")

    Range("S1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-1]=""MD NP"",RC[-1]=""MD New Patient""),""true"",""false"")"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 12), "S1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("R").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Select
    Selection.Cut Destination:=Columns("K")
    Columns("G").Select
    Selection.Cut Destination:=Columns("N")
    Columns("H").Select
    Selection.Cut Destination:=Columns("E")
    Columns("E").Select


    ' clean up phone cloumn
    Columns("E").Replace What:="~*", Replacement:=""
    Columns("E").Replace What:="Mobile: ", Replacement:=""
    Columns("E").Replace What:="Home: ", Replacement:=""
    Columns("E").Replace What:="Work: ", Replacement:=""
    Columns("E").Replace What:="Other: ", Replacement:=""
    Columns("E").Replace What:="Emergency: ", Replacement:=""

    'clean up phones
    Columns("E").Replace What:=" ", Replacement:=""
    Columns("E").Replace What:="-", Replacement:=""
    Columns("E").Replace What:="(", Replacement:=""
    Columns("E").Replace What:=")", Replacement:=""

    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=CLEAN(SUBSTITUTE(RC[-1],CHAR(10),"",""))"
    Columns("F").Select
    Selection.FillDown
    Columns("F").Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("E").Select
    Selection.TextToColumns Destination:=Range("E1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, OtherChar _
        :=",", FieldInfo:=Array(Array(1, 1), Array(2, 1), Array(3, 1)), _
        TrailingMinusNumbers:=True


    Rows("1").Select
    Selection.Delete Shift:=xlUp

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("I").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(LEFT(RC[-4],10),""+1 (###) ### ####"")"
    Range("I1").Select
    Selection.AutoFill Destination:=Range("I1:L1"), Type:=xlFillDefault
    Range("I1:L1").Select
    Columns("I:L").Select
    Selection.FillDown
    Columns("I:L").Copy
    Columns("I:L").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("E:H").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Select
    Selection.TextToColumns Destination:=Range("E1"), DataType:=xlDelimited, _
            TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
            Semicolon:=False, Comma:=False, Space:=False, Other:=False, OtherChar _
            :=",", FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    Columns("F").Select
    Selection.TextToColumns Destination:=Range("F1"), DataType:=xlDelimited, _
            TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
            Semicolon:=False, Comma:=False, Space:=False, Other:=False, OtherChar _
            :=",", FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    Columns("G").Select
    Selection.TextToColumns Destination:=Range("G1"), DataType:=xlDelimited, _
            TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
            Semicolon:=False, Comma:=False, Space:=False, Other:=False, OtherChar _
            :=",", FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
            TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
            Semicolon:=False, Comma:=False, Space:=False, Other:=False, OtherChar _
            :=",", FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    Columns("P").Select
    Selection.TextToColumns Destination:=Range("P1"), DataType:=xlDelimited, _
            TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
            Semicolon:=False, Comma:=False, Space:=False, Other:=False, OtherChar _
            :=",", FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O2").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown

    Columns("P").Replace What:=" ", Replacement:=""

    Range("O2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "P2").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

     Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

    
    
End Sub








