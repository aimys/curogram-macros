Sub Main()

Columns("B:C").Select
Selection.Cut Destination:=Columns("O:P")
Columns("F").Select
Selection.Cut Destination:=Columns("M")

Columns("A").Select
Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

Columns("A").Select
Selection.Cut Destination:=Columns("C")
Columns("B").Select
Selection.Cut Destination:=Columns("A")

Columns("O").Replace What:="/", Replacement:="."
Columns("D").Replace What:="/", Replacement:="."

Range("Q1").Activate
ActiveCell.FormulaR1C1 = "30"
Range("P1").End(xlDown).Select
Range(ActiveCell.Offset(0, 1), "Q1").Select
Selection.FillDown

Range("N1").Activate
ActiveCell.FormulaR1C1 = "Dr. Reza Salmassian"
Range("O1").End(xlDown).Select
Range(ActiveCell.Offset(0, -1), "N1").Select
Selection.FillDown

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")


    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"


    
End Sub


