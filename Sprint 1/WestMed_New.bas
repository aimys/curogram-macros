Sub Main()

    Columns("O").Select
    Selection.Cut Destination:=Columns("AZ")

    Range("O1").Select
    ActiveCell.FormulaR1C1 = "=SUBSTITUTE(RC[-1],""DNC"","""")"
    Range("L1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "O1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]=RC[-1],""1"","""")"
    Range("L1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 4), "P1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Selection.TextToColumns Destination:=Range("P1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    Columns("O:P").Select
    Selection.Delete Shift:=xlToLeft

    Columns("H").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("A").Select
    Selection.Cut Destination:=Columns("Y")

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Columns("C").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete
    
    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("C1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "E1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("M").Select
    Selection.Cut Destination:=Columns("Z")
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E").Select
    Selection.Cut Destination:=Columns("N")
    Columns("F:G").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("K:L").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I:J").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("L").Select
    Selection.Cut Destination:=Columns("R")
    Columns("H").Select
    Selection.Cut Destination:=Columns("L")
    Columns("K").Select
    Selection.Cut Destination:=Columns("I")

    Columns("R").Replace What:="Sleep Study - Baseline", Replacement:=""
    Columns("R").Replace What:="Sleep Study - HOME SLEEP TEST", Replacement:=""

    Columns("R").Replace What:="Surgery - Cash", Replacement:=""
    Columns("R").Replace What:="Surgery - Ins", Replacement:=""
    Columns("R").Replace What:="Surgery - Lien", Replacement:=""

    Columns("R").Replace What:="CT-Sinus Scan", Replacement:=""
    Columns("R").Replace What:="Pre-Op Testing", Replacement:=""
    Columns("R").Replace What:="Ultrasound", Replacement:=""

    Columns("R").Replace What:="Other", Replacement:=""

    Columns("R").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Range("S1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-1]=""Consult - Gastric Balloon"",RC[-1]=""Consult - New Patient (Ins)"",RC[-1]=""Consult - New Patient - Adjustment"",RC[-1]=""Consult - New Patient - No Seminar"",RC[-1]=""Consult - Other (Medical)"",RC[-1]=""Consult New Patient (Cash)"",RC[-1]=""Semiinar - PPO Max"",RC[-1]=""Seminar"",RC[-1]=""Seminar - Cash"",RC[-1]=""Seminar - Cash (with Insurance)"",RC[-1]=""Seminar - Cash Only"",RC[-1]=""Seminar - HMO"",RC[-1]=""Seminar - In Network Only"",RC[-1]=""Seminar - Insurance PPO"",RC[-1]=""Seminar - Medi-Care"",RC[-1]=""Seminar - New Ins"",RC[-1]=""Seminar - PPO Exclusion"",RC[-1]=""Seminar - PPO Max"",RC[-1]=""Seminar - PPO No Max""),""true"",""false"")"
    Range("R1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "S1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("R").Select
    Selection.Cut Destination:=Columns("M")
    Columns("S").Select
    Selection.Cut Destination:=Columns("R")

    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "P1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("O").Select
    Selection.Delete Shift:=xlToLeft

    'clean up phones
    Columns("E:F").Replace What:="(", Replacement:=""
    Columns("E:F").Replace What:="-", Replacement:=" "
    Columns("E:F").Replace What:=")", Replacement:=""

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O2").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown

    Range("G1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-2]=RC[-1],RC[-1]=""""),"""",RC[-1])"
    Range("D1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "G1").Select
    Selection.FillDown
    Selection.Copy
    Selection.Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("G").Select
    Selection.Cut Destination:=Columns("F")
    Columns("G").Select
    Selection.ClearContents

    Columns("V").Select
    Selection.Cut Destination:=Columns("K")

    Columns("K").Replace What:=",", Replacement:=""

    Columns("K").Select
    Selection.NumberFormat = "General"

    Columns("Q").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=TRIM(RC[-1])"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("P").Select
    Selection.Delete Shift:=xlToLeft

    Columns("P").Select
    Selection.TextToColumns Destination:=Range("P1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("P").Replace What:=" ", Replacement:=""

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")
    
    Columns("W").Select
    Selection.Cut Destination:=Columns("V")

    Columns("AV").Select
    Selection.Cut Destination:=Columns("O")
    
    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"
    
    ActiveSheet.Name = "WestMedNew"

    Range("A1").Select
    Range(Selection, Selection.End(xlToRight)).Select
    Range(Selection, Selection.End(xlDown)).Select
    ActiveWorkbook.Worksheets("WestMedNew").Sort.SortFields.Clear
    ActiveWorkbook.Worksheets("WestMedNew").Sort.SortFields.Add Key:=Columns("A"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("WestMedNew").Sort.SortFields.Add Key:=Columns("B"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("WestMedNew").Sort.SortFields.Add Key:=Columns("C"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("WestMedNew").Sort.SortFields.Add Key:=Columns("R"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("WestMedNew").Sort.SortFields.Add Key:=Columns("S"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    
    With ActiveWorkbook.Worksheets("WestMedNew").Sort
        .SetRange Columns("A:V")
        .Header = xlYes
        .MatchCase = False
        .Orientation = xlTopToBottom
        .SortMethod = xlPinYin
        .Apply
    End With


    Range("A1").Select
    Range(Selection, Selection.End(xlDown)).Select
    Range(Selection, Selection.End(xlToRight)).Select
    Selection.RemoveDuplicates Columns:=Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15, 16, 17, 18, 20, 21), Header:=xlYes


End Sub
