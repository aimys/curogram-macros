Sub Main()
    Columns("A:B").Select
    Selection.Cut Destination:=Columns("O:P")

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlFixedWidth, FieldInfo:=Array(Array(0, 1), Array(7, 9), Array(9, 1)), TrailingMinusNumbers:= True

    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    
    Columns("B").Select
    Selection.Cut Destination:=Columns("K")
    Columns("C:F").Select
    Selection.Cut Destination:=Columns("D:G")
    
    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]="""",RC[-3],CONCATENATE(RC[-2],"" "",RC[-1]))"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "E1").Select
    Selection.FillDown
    Selection.Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D").Select
    Selection.ClearContents

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=RC[-2],"""",RC[-2])"
    Range("E1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "D1").Select
    Selection.FillDown
    Selection.Copy
    Columns("B").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    Range("H1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=""New Patient"",""true"",""false"")"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "H1").Select
    Selection.FillDown
    Selection.Copy
    Columns("R").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("G").Select
    Selection.Cut Destination:=Columns("M")

    Columns("G:H").Select
    Selection.ClearContents

   Columns("K").Replace What:=")", Replacement:=""
   Columns("D").Replace What:="/", Replacement:="."

   Columns("E:F").Replace What:="(", Replacement:="+1 ("
   Columns("E:F").Replace What:="-", Replacement:=" "

   Columns("O:P").Replace What:="AM", Replacement:=" AM"
   Columns("O:P").Replace What:="PM", Replacement:=" PM"
    
   Range("Q1").Select
   ActiveCell.FormulaR1C1 = "=TEXT(ROUND((RC[-1]-RC[-2])*1440,0),""###"")"
   Range("R1").Select
   Selection.End(xlDown).Select
   Range(ActiveCell.Offset(0, -1), "Q1").Select
   Selection.FillDown
   Selection.Copy
   Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks:=False, Transpose:=False
   Application.CutCopyMode = False

   Columns("P").Select
   Selection.ClearContents

   Columns("O").Select
   Selection.Cut Destination:=Columns("P")

   Columns("N").Select
   Selection.Cut Destination:=Columns("O")

   ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub
