Sub Main()

    Columns("C:E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H:L").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("C").Select
    Selection.Cut Destination:=Columns("K")

    Columns("C").Select
    Selection.Delete Shift:=xlToLeft

    Columns("G").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("A:B").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("D").Select
    Selection.Cut Destination:=Columns("B")
    
    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 9), Array(2, 1)), TrailingMinusNumbers:=True
    Selection.Cut Destination:=Columns("D")
    
    Columns("C:D").Select
    Selection.Cut Destination:=Columns("B:C")
    Columns("E").Select
    Selection.Cut Destination:=Columns("D")
    Columns("H:I").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("J").Select
    Selection.Cut Destination:=Columns("K")

    'clean up phones
    Columns("E:F").Replace What:="-", Replacement:=" "
    Columns("E:F").Replace What:="(", Replacement:="+1 ("
    Columns("E:F").Replace What:=")", Replacement:=") "

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."
    
    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"

End Sub
