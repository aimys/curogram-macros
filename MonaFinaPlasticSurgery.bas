Sub Main()

    Columns("H").Select
    Selection.Cut Destination:=Columns("O")
    Columns("K").Select
    Selection.Cut Destination:=Columns("P")
    Columns("L").Select
    Selection.Cut Destination:=Columns("N")
    Columns("M").Select
    Selection.Cut Destination:=Columns("L")
    Columns("J").Select
    Selection.Cut Destination:=Columns("M")
    
    Columns("I").Select
    Selection.ClearContents

    Columns("A").Select
    Selection.Cut Destination:=Columns("K")
    Columns("C").Select
    Selection.Cut Destination:=Columns("I")
    Columns("G").Select
    Selection.Cut Destination:=Columns("C")
    Columns("C:G").Select
    Selection.Cut Destination:=Columns("D:H")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""",RC[-2],RC[-1])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0,3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("C").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=RC[-2],"""",RC[-2])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0,3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.ClearContents
    Columns("D").Select
    Selection.Cut Destination:=Columns("B")
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O2").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub
