Sub Main()

    Columns("B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E:I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F:O").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L").Select
    Selection.Delete Shift:=xlToLeft
    Columns("M:O").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("A").Select
    Selection.Cut Destination:=Columns("O")

    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="-", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("I").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("H").Select
    Selection.Cut Destination:=Columns("M")
    Columns("J").Select
    Selection.Cut Destination:=Columns("N")
    Columns("K").Select
    Selection.Cut Destination:=Columns("S")
    Columns("B").Select
    Selection.Cut Destination:=Columns("K")
    Columns("E").Select
    Selection.Cut Destination:=Columns("I")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("F:G").Select
    Selection.Cut Destination:=Columns("E:F")

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.Cut Destination:=Columns("C")

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-5]=""NEW PT"",""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("S").Select
    Selection.Cut Destination:=Columns("V")

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    Columns("A").Select
    Selection.Cut Destination:=Columns("B")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("B").Select
    Selection.Cut Destination:=Columns("C")

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")


    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

    Range("A1").Select
    Range(Selection, Selection.End(xlDown)).Select
    Range(Selection, Selection.End(xlToRight)).Select
    ActiveSheet.Columns("A:U").RemoveDuplicates Columns:=Array(1, 2, 3, 4, 5, 6, 7 , 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22), Header:=xlYes

End Sub