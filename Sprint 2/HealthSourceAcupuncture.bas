Sub Main()

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J:L").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K:R").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("A").Select
    Selection.Cut Destination:=Columns("L")
    Columns("J").Select
    Selection.Cut Destination:=Columns("T")
    Columns("T").Select
    Selection.Cut Destination:=Columns("U")
    Columns("B:C").Select
    Selection.Cut Destination:=Columns("R:S")
    Columns("E:G").Select
    Selection.Cut Destination:=Columns("A:C")
    Columns("D").Select
    Selection.Cut Destination:=Columns("K")

    Columns("H").Select
    Selection.Cut Destination:=Columns("N")
    Columns("I").Select
    Selection.Cut Destination:=Columns("Q")

    Columns("R:S").Select
    Selection.Cut Destination:=Columns("W:X")
    Columns("X").Select
    Selection.Cut Destination:=Columns("Y")

    Range("V1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=FALSE,""false"",""true"")"
    Range("U1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "V1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("U").Select
    Selection.Delete Shift:=xlToLeft

    Columns("W").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("W1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm/dd/yyyy"")"
    Range("V1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "W1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("X1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-2],""hh:mm AM/PM"")"
    Range("V1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "X1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("Z1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""hh:mm AM/PM"")"
    Range("Y1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Z1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("Y").Select
    Selection.Delete Shift:=xlToLeft
    Columns("V").Select
    Selection.Delete Shift:=xlToLeft

    Columns("V:W").Select
    Selection.Copy
    Columns("R:S").Select
    ActiveSheet.Paste
    Application.CutCopyMode = False
    
    Columns("V").Select
    Selection.Delete Shift:=xlToLeft

    Range("X1").Select
    ActiveCell.FormulaR1C1 = "=(RC[-1]-RC[-2])*1440"
    Range("W1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "X1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("X").Select
    Selection.Cut Destination:=Columns("T")
    
    Columns("V:W").Select
    Selection.Delete Shift:=xlToLeft

    Columns("R").Replace What:="/", Replacement:="."

    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub
