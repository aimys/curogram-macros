Sub Main()

    Rows("1:12").Select
    Selection.Delete Shift:=xlUp

    With ActiveWindow
        .SplitColumn = 0
        .SplitRow = 0
    End With

    Cells.Select
    ActiveWindow.DisplayGridlines = True
    Selection.UnMerge

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C:E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D:E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("B").Select
    Selection.Cut Destination:=Columns("F")

    Rows("1").Select
    Selection.Delete Shift:=xlUp
    
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "=SUBSTITUTE(RC[-1],CHAR(10),""."")"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 7), "G1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("G").Select
    Selection.TextToColumns Destination:=Range("G1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :=".", FieldInfo:=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 9)), _
        TrailingMinusNumbers:=True

    Columns("F").Select
    Selection.Delete Shift:=xlToLeft

    Range("B1").Select
    ActiveCell.FormulaR1C1 = "=R[1]C[-1]"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("C").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("A").Select
    Selection.Cut Destination:=Columns("I")

    Range("J1").Select
    ActiveCell.FormulaR1C1 = "=SUBSTITUTE(RC[-1],CHAR(10),"","")"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "J1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("J").Select
    Selection.TextToColumns Destination:=Range("J1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, OtherChar _
        :=".", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    
    Columns("J:K").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("C").Select
    Selection.Cut Destination:=Columns("N")
    Columns("D").Select
    Selection.Cut Destination:=Columns("L")
    Columns("I").Select
    Selection.ClearContents
    Columns("B").Select
    Selection.Cut Destination:=Columns("M")
    Columns("F").Select
    Selection.Cut Destination:=Columns("A")
    Columns("E").Select
    Selection.Cut Destination:=Columns("Q")

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, OtherChar _
        :=".", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.Cut Destination:=Columns("C")
    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("C").Select
    Selection.Cut Destination:=Columns("B")

    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[2]="""",RC[1],RC[2])"
    Range("B1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "C1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
  
    Columns("C").Select
    Selection.Cut Destination:=Columns("K")
    Columns("D").Select
    Selection.ClearContents
    Columns("E").Select
    Selection.ClearContents

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 9), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("A:C")
    Columns("G:H").Select
    Selection.Cut Destination:=Columns("E:F")

    'clean up phones
    Columns("E:H").Replace What:="(H) ", Replacement:=""
    Columns("E:H").Replace What:="(M) ", Replacement:=""
    Columns("E:H").Replace What:="(W) ", Replacement:=""
    Columns("E:H").Replace What:="-", Replacement:=" "
    Columns("E:H").Replace What:="(", Replacement:="+1 ("

    Columns("K").Replace What:=")", Replacement:=""

    Columns("O").Replace What:="/", Replacement:="."

    Columns("M").Select
    Selection.ClearContents

    Columns("Q").Select
    Selection.Cut Destination:=Columns("M")

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-7]=""New Patient"",RC[-7]=""New Patient 30min""),""true"",""false"")"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False


    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

    
End Sub
