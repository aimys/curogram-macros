Sub Main()

    Columns("E").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("C:D").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Cells.Select
    Selection.ClearFormats

    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[-3],2)=""Ph"","""",""A"")"
    Range("D1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "E1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("F1").Select
    ActiveCell.FormulaR1C1 = "=IF(R[1]C[-1]=""A"",CONCATENATE(RC[-5],"" "",R[1]C[-5]),RC[-5])"
    Range("D1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "F1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("G1").Select
    ActiveCell.FormulaR1C1 = "=IF(R[1]C[-2]=""A"",CONCATENATE(RC[-5],"" "",R[1]C[-5]),RC[-5])"
    Range("F1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "G1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("H1").Select
    ActiveCell.FormulaR1C1 = "=IF(R[1]C[-3]=""A"",CONCATENATE(RC[-5],"" "",R[1]C[-5]),RC[-5])"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "H1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("I1").Select
    ActiveCell.FormulaR1C1 = "=IF(R[1]C[-4]=""A"",CONCATENATE(RC[-5],"" "",R[1]C[-5]),RC[-5])"
    Range("H1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "I1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A:D").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("A1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[1]=""A"","""",""A"")"
    Range("C1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -2), "A1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("A").Select    
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("A:B").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 9), Array(7, 1), Array(20, 9)), TrailingMinusNumbers:= _
        True

    Columns("C").Select
    Selection.Cut Destination:=Columns("E")

    Columns("E").Replace What:="Unscheduled", Replacement:=""

    Columns("E").Select
    Selection.TextToColumns Destination:=Range("E1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 1), Array(2, 1), Array(3, 1)), _
        TrailingMinusNumbers:=True

    Columns("F").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Range("H1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(TEXT(RC[-2],""hh:mm""),RC[-1])"
    Range("G1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "H1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("F:G").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E:F").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("D").Select
    Selection.Cut Destination:=Columns("N")
    Columns("B").Select
    Selection.Cut Destination:=Columns("E")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    
    Columns("B").Select
    Selection.Cut Destination:=Columns("K")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True


    Columns("A").Select
    Selection.Cut Destination:=Columns("C")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, OtherChar _
        :="", FieldInfo:=Array(Array(1, 9), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("C").Select
    Selection.Cut Destination:=Columns("B")

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, OtherChar _
        :="(", FieldInfo:=Array(1, 1), TrailingMinusNumbers:=True

    Columns("D").Select
    Selection.ClearContents

    Columns("B").Select
    Selection.Cut Destination:=Columns("D")
    Columns("C:D").Select
    Selection.Cut Destination:=Columns("B:C")

    Columns("K").Replace What:=")", Replacement:=""

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")


    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

    Cells.Select
    ActiveSheet.Columns("A:U").RemoveDuplicates Columns:=Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21), Header:=xlYes

End Sub
