Sub Main()

    'Formatting Data

    Cells.Select
    Selection.ClearFormats
    With Selection.Font
        .ThemeColor = xlThemeColorLight1
        .TintAndShade = 0
    End With

    Cells.Select
    With Selection.Interior
        .Pattern = xlNone
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With

    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("C").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Rows("1").Select
    Selection.Delete Shift:=xlUp
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[4]=""SR"","""",RC[-1])"
    Columns("C").Select
    Selection.FillDown
    Columns("C").Copy
    Columns("C").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Replace What:="0", Replacement:=""
    
    Columns("B").Select
    Selection.Delete Shift:=xlToLeft

    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]=""-"",RC[-1],"""")"
    Range("B1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "C1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.TextToColumns Destination:=Range("C1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("C4").Activate
    Selection.FillDown
    Columns("C").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    ' Columns("D").Select
    ' Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    ' Range("D1").Select
    ' ActiveCell.FormulaR1C1 = "=IF(AND(RC[-1]=""APPT MADE"",RC[-2]=""VERIFIED""),""1"","""")"
    ' Range("C1").Select
    ' Selection.End(xlDown).Select
    ' Range(ActiveCell.Offset(0, 1), "D1").Select
    ' Selection.FillDown
    ' Columns("D").Select
    ' Selection.Copy
    ' Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    '     :=False, Transpose:=False
    ' Application.CutCopyMode = False

    ' Columns("D").Select
    ' Selection.TextToColumns Destination:=Range("D1"), DataType:=xlDelimited, _
    '     TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
    '     Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
    '     :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("D").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    ' Columns("C").Select
    ' Selection.ClearContents
    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Columns("F").Select
    Selection.TextToColumns Destination:=Range("F1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    
    Columns("F").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("A:B").Select
    Selection.Cut Destination:=Columns("AY:AZ")

    Columns("A:B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F:G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H:I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J:N").Select
    Selection.Delete Shift:=xlToLeft

    Columns("I").Select
    Selection.Cut Destination:=Columns("M")
    Columns("B").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("C").Select
    Selection.Cut Destination:=Columns("O")
    Columns("G").Select
    Selection.Cut Destination:=Columns("L")
    Columns("F").Select
    Selection.Cut Destination:=Columns("K")
    Columns("D:E").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("H").Select
    Selection.Cut Destination:=Columns("A")
    
    Columns("B:C").Select
    Selection.ClearContents

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="^", FieldInfo:=Array(Array(1, 1), Array(2, 1), Array(3, 1)), _
        TrailingMinusNumbers:=True

    
    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    Columns("C").Select
    Selection.Cut Destination:=Columns("B")
    Columns("D").Select
    Selection.Cut Destination:=Columns("C")

    Columns("D:F").Select
    Selection.Delete Shift:=xlToLeft

    Columns("Q").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
    Columns("P").Select
    Selection.FillDown
    Columns("P").Copy
    Columns("P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]="""","""",TEXT(RC[-2],""hh:mm AM/PM""))"
    Columns("Q").Select
    Selection.FillDown
    Columns("Q").Copy
    Columns("Q").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("O").Select
    Selection.Delete Shift:=xlToLeft

    'clean up phones
     Columns("E:F").Replace What:="(", Replacement:="+1 ("
     Columns("E:F").Replace What:=")", Replacement:=") "
     Columns("E:F").Replace What:="-", Replacement:=" "

    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, OtherChar _
        :="", TrailingMinusNumbers:=True

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")

    Range("AM2").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-1],""-"",RC[-2])"
    Range("AL2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "AM2").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("AK:AL").Select
    Selection.Delete Shift:=xlToLeft

    Columns("AK").Select
    Selection.Cut Destination:=Columns("V")

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("V1").Select
    ActiveCell.FormulaR1C1 = "emrStatus"

    activesheet.name = "AdvancedShoreImagingAssociates_"

    ActiveWorkbook.Worksheets("AdvancedShoreImagingAssociates_").Sort.SortFields.Clear
    ActiveWorkbook.Worksheets("AdvancedShoreImagingAssociates_").Sort.SortFields.Add Key:=Columns("C"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("AdvancedShoreImagingAssociates_").Sort.SortFields.Add Key:=Columns("B"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("AdvancedShoreImagingAssociates_").Sort.SortFields.Add Key:=Columns("A"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("AdvancedShoreImagingAssociates_").Sort.SortFields.Add Key:=Columns("O"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("AdvancedShoreImagingAssociates_").Sort.SortFields.Add Key:=Columns("P"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    
    With ActiveWorkbook.Worksheets("AdvancedShoreImagingAssociates_").Sort
        .SetRange Columns("A:U")
        .Header = xlYes
        .MatchCase = False
        .Orientation = xlTopToBottom
        .SortMethod = xlPinYin
        .Apply
    End With
    
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Range("D2").Select
    ActiveCell.FormulaR1C1 = "=IF(AND(RC[-1]=R[-1]C[-1],RC[-3]=R[-1]C[-3]),"""",TRIM(RC[-1]))"
    
    Range("C2").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "D2").Select
    Selection.FillDown
    Columns("D").Copy
    Columns("D").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("D1").Activate
    ActiveCell.FormulaR1C1 = "30"

    Columns("D").Select
    Selection.TextToColumns Destination:=Range("D1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, OtherChar _
        :="", TrailingMinusNumbers:=True

    Columns("D").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("D").Select
    Selection.Delete Shift:=xlToLeft

    Range("A1").Select
    Selection.Copy
    Cells.Select
    Selection.PasteSpecial Paste:=xlPasteFormats, Operation:=xlNone, SkipBlanks:=False, Transpose:=False
    Application.CutCopyMode = False

End Sub
