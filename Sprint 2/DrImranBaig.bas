Sub Main()

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J:N").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L:Y").Select
    Selection.Delete Shift:=xlToLeft
    Columns("O:S").Select
    Selection.Delete Shift:=xlToLeft

    Columns("G:H").Select
    Selection.Cut Destination:=Columns("R:S")
    Columns("A").Select
    Selection.Cut Destination:=Columns("G")
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("A:C")
    Columns("E").Select
    Selection.Cut Destination:=Columns("D")
    Columns("G").Select
    Selection.Cut Destination:=Columns("H")
    Columns("F").Select
    Selection.Cut Destination:=Columns("G")
    Columns("J:K").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("M").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("L").Select
    Selection.Cut Destination:=Columns("O")
    Columns("G").Select
    Selection.Cut Destination:=Columns("L")
    Columns("H").Select
    Selection.Cut Destination:=Columns("K")
    
    Columns("O").Select
    Selection.ClearContents
    
    Columns("N").Select
    Selection.Cut Destination:=Columns("O")
    Columns("I").Select
    Selection.Cut Destination:=Columns("N")

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("T1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("S1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "T1").Select
    Selection.FillDown

    Columns("R").Replace What:="/", Replacement:="."
    Columns("D").Replace What:="/", Replacement:="."

    Columns("S").Replace What:=" ", Replacement:=""

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-7]=""NV"",""true"",""false"")"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub

