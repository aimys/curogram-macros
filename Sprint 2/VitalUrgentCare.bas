Sub Main()
    Columns("O").Select
    Selection.Delete Shift:=xlToLeft
    Columns("P:R").Select
    Selection.Delete Shift:=xlToLeft

    Columns("Q").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("H").Select
    Selection.Cut Destination:=Columns("Q")

    Columns("R:U").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft

    Columns("F:L").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I:K").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L:X").Select
    Selection.Delete Shift:=xlToLeft

    Columns("K").Select
    Selection.Cut Destination:=Columns("N")
    Columns("J").Select
    Selection.Cut Destination:=Columns("L")
    Columns("I").Select
    Selection.Cut Destination:=Columns("O")
    Columns("A").Select
    Selection.Cut Destination:=Columns("K")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("B").Select
    Selection.Cut Destination:=Columns("C")
    Columns("D").Select
    Selection.Cut Destination:=Columns("B")
    Columns("E").Select
    Selection.Cut Destination:=Columns("D")
    Columns("F:G").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("H").Select
    Selection.Cut Destination:=Columns("I")
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown

    Range("P1").Select
    ActiveCell.FormulaR1C1 = "05:00PM"
    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "P1").Select
    Selection.FillDown

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

    
End Sub
