Sub Macro1()
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("A:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("B:C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("A").Select
    Selection.Cut Destination:=Columns("N")
    Columns("B:C").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("D").Select
    Selection.Cut Destination:=Columns("M")
    Columns("E").Select
    Selection.Cut Destination:=Columns("K")
    Columns("F").Select
    Selection.Cut Destination:=Columns("A")

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 9), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True
    
    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("A:C")
    Columns("H").Select
    Selection.Cut Destination:=Columns("D")
    Columns("G").Select
    Selection.Cut Destination:=Columns("E")
    Columns("I").Select
    Selection.Cut Destination:=Columns("F")

    'clean up phones
    Columns("E:H").Replace What:="-", Replacement:=" "
    Columns("E:H").Replace What:="(", Replacement:="+1 ("

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 1), Array(10, 9)), TrailingMinusNumbers:=True

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("M").Replace What:="BLOCK", Replacement:=""
    Columns("M").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-5]=""NP"",""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub
