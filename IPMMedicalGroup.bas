Sub Main()

    Columns("B:C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F:K").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H:L").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I:K").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J:P").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K:O").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L:R").Select
    Selection.Delete Shift:=xlToLeft
    Columns("N:U").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K").Select
    Selection.Delete Shift:=xlToLeft

    Columns("K:L").Select
    Selection.Cut Destination:=Columns("S:T")
    Columns("B:C").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("H").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("E").Select
    Selection.Cut Destination:=Columns("M")
    Columns("A").Select
    Selection.Cut Destination:=Columns("L")
    Columns("D").Select
    Selection.Cut Destination:=Columns("N")
    Columns("F").Select
    Selection.Cut Destination:=Columns("A")
    Columns("G").Select
    Selection.Cut Destination:=Columns("D")
    Columns("I").Select
    Selection.Cut Destination:=Columns("E")
    Columns("K").Select
    Selection.Cut Destination:=Columns("F")
    Columns("J").Select
    Selection.Cut Destination:=Columns("K")
    Columns("S:T").Select
    Selection.Cut Destination:=Columns("F:G")

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=True, OtherChar _
        :="(", FieldInfo:=Array(Array(1, 1), Array(2, 9)), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.ClearContents

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 9), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("A").Select
    Selection.Cut Destination:=Columns("D")
    
    Columns("A").Select
    Selection.Delete Shift:=xlToLeft

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-5]=""NP/PA Follow Up "",RC[-5]=""New Patient""),""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    
    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    Columns("Q").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""hh:mm AM/PM"")"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("P").Select
    Selection.Delete Shift:=xlToLeft    

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
End Sub
