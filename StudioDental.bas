Attribute VB_Name = "Module1"
Sub Main()
'Deleting Age Column
Columns("C").Select
Selection.ClearContents

'Moving Appointment Date Column
Columns("A").Select
Selection.Cut Destination:=Columns("C")

'Moving Appointment Date,Time,Duration Columns
Columns("C:E").Select
Selection.Cut Destination:=Columns("O:Q")

'Deleting Description Column
Columns("F").Select
Selection.ClearContents

'Moving Name Column To 1st Column
Columns("B").Select
Selection.Cut Destination:=Columns("A")

'Splitting Name Column in firstname and lastname
Columns("A").Select
Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
        
'moving lastname column
Columns("B").Select
Selection.Cut Destination:=Columns("C")

Columns("A").Select
Selection.Cut Destination:=Columns("B")
Columns("C").Select
Selection.Cut Destination:=Columns("A")
Columns("B").Select
Selection.Cut Destination:=Columns("C")
Columns("C").Select

'moving phone columns
Columns("G:I").Select
Selection.Cut Destination:=Columns("E:G")

'removing header row
Rows("1").Select
Selection.Delete Shift:=xlUp

'formatting appointment date column
Columns("P").Select
Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
Range("P1").Select
ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""","""",TEXT(RC[-1],""mm.dd.yyyy""))"
Columns("P").Select
Selection.FillDown
Columns("P").Copy
Columns("P").Select
Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    :=False, Transpose:=False
Application.CutCopyMode = False
Columns("O").Select
Selection.Delete Shift:=xlToLeft

Columns("O").Select
Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(1, 1), TrailingMinusNumbers:=True

'clean up phones
Columns("E:G").Replace What:=" ", Replacement:=""
Columns("E:G").Replace What:="-", Replacement:=""
Columns("E:G").Replace What:="(", Replacement:=""
Columns("E:G").Replace What:=")", Replacement:=""

'Formatting Phone Columns
Range("H1").Select
ActiveCell.FormulaR1C1 = _
    "=IF(RC[-3]="""","""",TEXT(RC[-3],""+1 (###) ### ####""))"
Range("H1").Select
Selection.AutoFill Destination:=Range("H1:J1"), Type:=xlFillDefault
Range("H1:J1").Select
Columns("H").Select
Selection.FillDown
Columns("I").Select
Selection.FillDown
Columns("J").Select
Selection.FillDown
Columns("H:J").Select
Columns("H:J").Copy
Columns("E:G").Select
Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
    :=False, Transpose:=False
Application.CutCopyMode = False
Columns("H:J").Select
Selection.ClearContents

Columns("E").Select
Selection.TextToColumns Destination:=Range("E1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(1, 1), TrailingMinusNumbers:=True
    
Columns("F").Select
Selection.TextToColumns Destination:=Range("F1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(1, 1), TrailingMinusNumbers:=True
    
Columns("G").Select
Selection.TextToColumns Destination:=Range("G1"), DataType:=xlDelimited, _
    TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
    Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
    :=Array(1, 1), TrailingMinusNumbers:=True

' Adding Header Row
Rows("1:1").Select
Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

' Changing Header Names
ActiveCell.FormulaR1C1 = ""
Range("A1").Select
ActiveCell.FormulaR1C1 = "firstName"
Range("B1").Select
ActiveCell.FormulaR1C1 = "middleName"
Range("C1").Select
ActiveCell.FormulaR1C1 = "lastName"
Range("D1").Select
ActiveCell.FormulaR1C1 = "dob"
Range("E1").Select
ActiveCell.FormulaR1C1 = "phone1"
Range("F1").Select
ActiveCell.FormulaR1C1 = "phone2"
Range("G1").Select
ActiveCell.FormulaR1C1 = "phone3"
Range("H1").Select
ActiveCell.FormulaR1C1 = "phone4"
Range("I1").Select
ActiveCell.FormulaR1C1 = "email1"
Range("J1").Select
ActiveCell.FormulaR1C1 = "email2"
Range("K1").Select
ActiveCell.FormulaR1C1 = "mrn"
Range("L1").Select
ActiveCell.FormulaR1C1 = "address"
Range("M1").Select
ActiveCell.FormulaR1C1 = "service"
Range("N1").Select
ActiveCell.FormulaR1C1 = "physicianName"
Range("O1").Select
ActiveCell.FormulaR1C1 = "appointmentDate"
Range("P1").Select
ActiveCell.FormulaR1C1 = "appointmentTime"
Range("Q1").Select
ActiveCell.FormulaR1C1 = "duration"
Range("L2").Select
    
End Sub




