Sub Main()
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    'Deleting Columns
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft

    Columns("I").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=""NP"",""true"",""false"")"
    Range("H1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "I1").Select
    Selection.FillDown
    Selection.Copy
    Columns("AV").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("H").Select
    Selection.Cut Destination:=Columns("AZ")

    Columns("I").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("O:AC").Select
    Selection.Delete Shift:=xlToLeft
    Columns("P:U").Select
    Selection.Delete Shift:=xlToLeft
    
    'Replacing Columns
    Columns("F:L").Select
    Selection.Cut Destination:=Columns("P:V")
    Columns("M:N").Select
    Selection.Cut Destination:=Columns("F:G")
    Columns("A").Select
    Selection.Cut Destination:=Columns("L")
    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L:M").Select
    Selection.Delete Shift:=xlToLeft
    
    'Formatting dob Column
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("D1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],"" "",""/""),""mm.dd.yyyy""))"
    Columns("D").Select
    Selection.FillDown
    Selection.Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("D").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Concatinating Address Column
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-5],"" "",RC[-4],"" "",RC[-3],"" "",RC[-2],"" "",RC[-1])"
    Columns("T").Select
    Selection.FillDown
    Selection.Copy
    Columns("U").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("O:T").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Replacing Columns
    Columns("N").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("L").Select
    Selection.Cut Destination:=Columns("N")
    Columns("P").Select
    Selection.Cut Destination:=Columns("L")
    Columns("L").Select
    Columns("O").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("M").Select
    Selection.Cut Destination:=Columns("O")
    Columns("O").Select
    
    'Formatting Phone1 Columns
    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],""-"",""""),""+1 (###) ### ####""))"
    Columns("E").Select
    Selection.FillDown
    Selection.Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("E").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Formatting Phone2 Columns
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],""-"",""""),""+1 (###) ### ####""))"
    Columns("F").Select
    Selection.FillDown
    Selection.Copy
    Columns("G").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("F").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Formatting appointment date Column
    Columns("O").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("O1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],"" "",""/""),""mm.dd.yyyy""))"
    Columns("O").Select
    Selection.FillDown
    Selection.Copy
    Columns("P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("O").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Refreshing Grid
    Columns("D").Select
    Selection.TextToColumns Destination:=Range("D1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
        
    Columns("E").Select
    Selection.TextToColumns Destination:=Range("E1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1)), TrailingMinusNumbers:= _
        True
        
    Columns("F").Select
    Selection.TextToColumns Destination:=Range("F1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1)), TrailingMinusNumbers:= _
        True
    
    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("L").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    ActiveCell.FormulaR1C1 = "=TRIM(RC[1])"
    Columns("L").Select
    Selection.FillDown
    Selection.Copy
    Columns("M").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("L").Select
    Selection.Delete Shift:=xlToLeft

    Columns("L").Select
    Selection.TextToColumns Destination:=Range("L1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O2").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown

    Columns("V").Select
    Selection.Cut Destination:=Columns("M")
    
    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")


    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    
End Sub
