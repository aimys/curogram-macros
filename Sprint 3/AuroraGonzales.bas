Sub Main()

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J:N").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L:Z").Select
    Selection.Delete Shift:=xlToLeft
    Columns("M:R").Select
    Selection.Delete Shift:=xlToLeft

    Columns("K").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("A").Select
    Selection.Cut Destination:=Columns("K")
    Columns("B:E").Select
    Selection.Cut Destination:=Columns("A:D")
    Columns("J").Select
    Selection.Cut Destination:=Columns("E")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")
    Columns("I").Select
    Selection.Cut Destination:=Columns("M")
    Columns("G:H").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("F").Select
    Selection.Cut Destination:=Columns("J")
    Columns("L").Select
    Selection.Cut Destination:=Columns("F")
    Columns("J").Select
    Selection.Cut Destination:=Columns("L")
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-5]=""NPGYN"",""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("O").Select
    Selection.Copy
    Range("U1").Select
    ActiveSheet.Paste
    Application.CutCopyMode = False

    Columns("U").Replace What:=".", Replacement:="/"

    Range("V1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""dddd"")"
    Range("U1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "V1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("U").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("N").Select
    Selection.Copy
    Range("V1").Select
    ActiveSheet.Paste
    Application.CutCopyMode = False

    Range("W1").Select
    ActiveCell.FormulaR1C1 = "=TRIM(RC[-1])"
    Range("V1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "W1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("V").Select
    Selection.Delete Shift:=xlToLeft

    Range("W1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-2]=""Monday"",IF(OR(RC[-1]=""GONZALEZ AURORA"",RC[-1]=""VILA-WRIGHT SHARON"",RC[-1]=""VILA-WRIGHT SHARON Y"",RC[-1]=""KAO LYDIA"",RC[-1]=""WHITELOCK JENNIFER"",RC[-1]=""CHAIRES VASQUEZ VIOLETTA""),""Texas Medical Center Doctors"",IF(OR(RC[-1]=""GOLDEN LINDSEY G"",RC[-1]=""HUYNH LINDA""),""Pearland Doctors"","""")),"""")"
    Range("V1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "W1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("X1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-3]=""Tuesday"",IF(OR(RC[-2]=""VILA-WRIGHT SHARON"",RC[-2]=""VILA-WRIGHT SHARON Y"",RC[-2]=""GOLDEN LINDSEY G"",RC[-2]=""HUYNH LINDA"",RC[-2]=""KAO LYDIA"",RC[-2]=""WHITELOCK JENNIFER""),""Texas Medical Center Doctors"",IF(OR(RC[-2]=""GONZALEZ AURORA"",RC[-2]=""WHITELOCK JENNIFER"",RC[-2]=""CHAIRES VASQUEZ VIOLETTA""),""Pearland Doctors"","""")),"""")"
    Range("V1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "X1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("Y1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-4]=""Wednesday"",IF(OR(RC[-3]=""VILA-WRIGHT SHARON"",RC[-3]=""VILA-WRIGHT SHARON Y"",RC[-3]=""GOLDEN LINDSEY G"",RC[-3]=""WHITELOCK JENNIFER"",RC[-3]=""KAO LYDIA"",RC[-3]=""CHAIRES VASQUEZ VIOLETTA""),""Texas Medical Center Doctors"",IF(OR(RC[-3]=""GONZALEZ AURORA"",RC[-3]=""HUYNH LINDA""),""Pearland Doctors"","""")),"""")"
    Range("V1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "Y1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("Z1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-5]=""Thursday"",IF(OR(RC[-4]=""VILA-WRIGHT SHARON"",RC[-4]=""VILA-WRIGHT SHARON Y"",RC[-4]=""GONZALEZ AURORA"",RC[-4]=""HUYNH LINDA"",RC[-4]=""KAO LYDIA"",RC[-4]=""CHAIRES VASQUEZ VIOLETTA""),""Texas Medical Center Doctors"",IF(OR(RC[-4]=""GOLDEN LINDSEY G"",RC[-4]=""WHITELOCK JENNIFER""),""Pearland Doctors"","""")),"""")"
    Range("V1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 4), "Z1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("AA1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-6]=""Friday"",IF(OR(RC[-5]=""VILA-WRIGHT SHARON"",RC[-5]=""VILA-WRIGHT SHARON Y"",RC[-5]=""GOLDEN LINDSEY G"",RC[-5]=""GONZALEZ AURORA"",RC[-5]=""CHAIRES VASQUEZ VIOLETTA""),""Texas Medical Center Doctors"",""""),"""")"
    Range("V1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 5), "AA1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("AB1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-5],RC[-4],RC[-3],RC[-2],RC[-1])"
    Range("V1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 6), "AB1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("W:AA").Select
    Selection.Delete Shift:=xlToLeft

    Columns("U:V").Select
    Selection.ClearContents
    Columns("L").Select
    Selection.ClearContents

    Columns("W").Select
    Selection.Cut Destination:=Columns("L")

    Columns("L").Select
    Selection.TextToColumns Destination:=Range("L1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")


    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

    ActiveSheet.Name = "AuroraGonzales"

    Range("A1").Select
    Range(Selection, Selection.End(xlToRight)).Select
    Range(Selection, Selection.End(xlDown)).Select
    ActiveWorkbook.Worksheets("AuroraGonzales").Sort.SortFields.Clear
    ActiveWorkbook.Worksheets("AuroraGonzales").Sort.SortFields.Add Key:=Columns("A"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("AuroraGonzales").Sort.SortFields.Add Key:=Columns("B"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("AuroraGonzales").Sort.SortFields.Add Key:=Columns("C"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("AuroraGonzales").Sort.SortFields.Add Key:=Columns("R"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    ActiveWorkbook.Worksheets("AuroraGonzales").Sort.SortFields.Add Key:=Columns("S"), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
    
    With ActiveWorkbook.Worksheets("AuroraGonzales").Sort
        .SetRange Columns("A:U")
        .Header = xlYes
        .MatchCase = False
        .Orientation = xlTopToBottom
        .SortMethod = xlPinYin
        .Apply
    End With

    Range("A1").Select
    Range(Selection, Selection.End(xlDown)).Select
    Range(Selection, Selection.End(xlToRight)).Select
    Selection.RemoveDuplicates Columns:=Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 20, 21), Header:=xlYes


End Sub
