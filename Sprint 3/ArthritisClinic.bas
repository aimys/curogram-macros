Sub Main()

    Columns("M").Select
    Selection.Cut Destination:=Columns("AZ")

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I:M").Select
    Selection.Delete Shift:=xlToLeft
    Columns("K:Y").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L:Q").Select
    Selection.Delete Shift:=xlToLeft

    Columns("F:G").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("K").Select
    Selection.Cut Destination:=Columns("N")
    Columns("A").Select
    Selection.Cut Destination:=Columns("K")
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("A:C")
    Columns("E").Select
    Selection.Cut Destination:=Columns("D")
    Columns("H").Select
    Selection.Cut Destination:=Columns("M")
    Columns("I:J").Select
    Selection.Cut Destination:=Columns("E:F")
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-5]=""New Pt"",""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("N:P").Select
    Selection.Copy
    Range("U1").Select
    ActiveSheet.Paste
    Application.CutCopyMode = False

    Columns("W").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("W1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""dddd"")"
    Range("V1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "W1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("X").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("X1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[1],""hh:mm AM/PM"")"
    Range("W1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "X1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("Y").Select
    Selection.Delete Shift:=xlToLeft
    Columns("U").Select
    Selection.Cut Destination:=Columns("Z")
    Columns("T:V").Select
    Selection.Delete Shift:=xlToLeft

    Columns("V").Select
    Selection.Delete Shift:=xlToLeft

    Columns("U").Select
    Selection.TextToColumns Destination:=Range("U1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Range("X1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-4]=""Monday"",IF(OR(TRIM(RC[-2])=""Malani Seema"",TRIM(RC[-2])=""Kugasia Aman S""),IF(AND(RC[-3]>=TIMEVALUE(""08:00 AM""),RC[-3]<=TIMEVALUE(""05:00 PM"")),""Katy"",""""),""""),"""")"
    Range("W1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "X1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("Y1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-5]=""Tuesday"",IF(AND(TRIM(RC[-3])=""Malani Seema"",RC[-4]>=TIMEVALUE(""08:00 AM""),RC[-4]<=TIMEVALUE(""11:00 AM"")),""Katy"",IF(AND(TRIM(RC[-3])=""Malani Seema"",RC[-4]>=TIMEVALUE(""01:00 PM""),RC[-4]<=TIMEVALUE(""05:00 PM"")),""Cypress"",IF(AND(TRIM(RC[-3])=""Kugasia Aman S"",RC[-4]>=TIMEVALUE(""08:00 AM""),RC[-4]<=TIMEVALUE(""11:00 AM"")),""Cypress"",IF(AND(TRIM(RC[-3])=""Kugasia Aman S"",RC[-4]>=TIMEVALUE(""01:00 PM""),RC[-4]<=TIMEVALUE(""05:00 PM"")),""Katy"","""")))),"""")"
    Range("W1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Y1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("Z1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-6]=""Wednesday"",IF(OR(TRIM(RC[-4])=""Malani Seema"",TRIM(RC[-4])=""Kugasia Aman S""),IF(AND(RC[-5]>=TIMEVALUE(""08:00 AM""),RC[-5]<=TIMEVALUE(""05:00 PM"")),""Katy"",""""),""""),"""")"
    Range("W1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "Z1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("AA1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-7]=""Thursday"",IF(AND(TRIM(RC[-5])=""Malani Seema"",RC[-6]>=TIMEVALUE(""09:00 AM""),RC[-6]<=TIMEVALUE(""04:00 PM"")),""Humble"",IF(AND(TRIM(RC[-5])=""Kugasia Aman S"",RC[-6]>=TIMEVALUE(""08:00 AM""),RC[-6]<=TIMEVALUE(""05:00 PM"")),""Katy"","""")),"""")"
    Range("W1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 4), "AA1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Range("AB1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-8]=""Friday"",IF(AND(TRIM(RC[-6])=""Malani Seema"",RC[-7]>=TIMEVALUE(""08:00 AM""),RC[-7]<=TIMEVALUE(""05:00 PM"")),""Katy"",IF(AND(TRIM(RC[-6])=""Kugasia Aman S"",RC[-7]>=TIMEVALUE(""09:00 AM""),RC[-7]<=TIMEVALUE(""03:30 PM"")),""Humble"","""")),"""")"
    Range("W1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 5), "AB1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Range("AC1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-5],RC[-4],RC[-3],RC[-2],RC[-1])"
    Range("W1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 6), "AC1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("X:AB").Select
    Selection.Delete Shift:=xlToLeft

    Columns("X").Select
    Selection.Cut Destination:=Columns("L")

    Columns("S:X").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    Columns("O:R").Select
    Selection.Cut Destination:=Columns("R:U")
    Columns("N").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")

    Columns("S").Replace What:=" ", Replacement:=""

    Columns("L").Select
    Selection.Cut Destination:=Columns("M")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("O")

    Columns("M").Select
    Selection.TextToColumns Destination:=Range("M1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("M").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "emrlocation"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "emrProvider"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "language"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("U1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub
