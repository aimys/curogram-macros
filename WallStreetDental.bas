Sub Main()

    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("A1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[1],5)=""Table"",R[1]C[1],"""")"
    Range("B1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "A1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("A3").Activate
    Selection.FillDown
    Columns("A").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("F").Select
    Selection.Delete Shift:=xlToLeft

    Columns("B").Replace What:="a", Replacement:=" AM"
    Columns("B").Replace What:="p", Replacement:=" PM"

    Columns("A:B").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("E").Select
    Selection.Cut Destination:=Columns("Q")
    Columns("D").Select
    Selection.Cut Destination:=Columns("E")
 
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.Cut Destination:=Columns("C")

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1)), TrailingMinusNumbers:=True

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""",RC[-3],RC[-1])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("E").Select
    Selection.Cut Destination:=Columns("A")

    Range("E1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=RC[-4],RC[-3],"""")"
    Range("B1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "E1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    Columns("B").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-1],"" "",RC[-2])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B:C").Select
    Selection.Delete Shift:=xlToLeft
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("D").Select
    Selection.Cut Destination:=Columns("C")

    Columns("A:C").Replace What:=" ", Replacement:=""


    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=False, Comma:=True, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True
    
    Columns("O").Select
    Selection.Cut Destination:=Columns("V")

    Range("O1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(TRIM(RC[1]),"", "",RC[2])"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "O1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("P:Q").Select
    Selection.Delete Shift:=xlToLeft

    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "P1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("O").Select
    Selection.Delete Shift:=xlToLeft

    Range("U1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-1]=""Monday"",RC[-1]=""Thursday"",RC[-1]=""Friday""),""Downtown"",IF(OR(RC[-1]=""Tuesday"",RC[-1]=""Wednesday""),""Beverly Hills"",""""))"
    Range("T1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "U1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("U").Select
    Selection.Cut Destination:=Columns("L")
    Columns("T").Select
    Selection.Delete Shift:=xlToLeft

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"


End Sub
