Sub Main()

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Columns("K").Select
    Selection.Cut Destination:=Columns("S")

    Columns("K:L").Select
    Selection.Delete Shift:=xlToLeft

    Columns("Q").Select
    Selection.Cut Destination:=Columns("S")
    Columns("L:N").Select
    Selection.Cut Destination:=Columns("O:Q")
    Columns("K").Select
    Selection.Cut Destination:=Columns("N")
    Columns("J").Select
    Selection.Cut Destination:=Columns("M")
    Columns("I").Select
    Selection.Cut Destination:=Columns("L")
    Columns("H").Select
    Selection.Cut Destination:=Columns("K")
    Columns("G").Select
    Selection.Cut Destination:=Columns("I")
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    'clean up phones
    Columns("E:G").Replace What:="-", Replacement:=" "
    Columns("E:G").Replace What:="(", Replacement:="+1 ("
    Columns("E:G").Replace What:=")", Replacement:=") "

    Columns("D").Replace What:="/", Replacement:="."
    Columns("P").Replace What:="/", Replacement:="."

    Columns("I").Replace What:="No Email", Replacement:=""
    Columns("S").Replace What:="x - Cancelled", Replacement:=""

    Columns("S").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("S").Select
    Selection.Delete Shift:=xlToLeft

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(RC[-5]=""NEW PATIENT 30 MINUTES"",RC[-5]=""GI NEW PATIENT"",RC[-5]=""RHEUM NP"",RC[-5]=""PM NEW PATIENT""),""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

    ActiveSheet.Columns("A:R").RemoveDuplicates Columns:=Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 17, 18), Header:=xlYes

End Sub