Sub Main()

    Range("H1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-7],RC[-6],RC[-5],RC[-4],RC[-3],RC[-2],RC[-1])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 7), "H1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A:G").Select
    Selection.Delete Shift:=xlToLeft
'
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(LEFT(RC[-1],2)=""H1"",LEFT(RC[-1],2)=""H2"",LEFT(RC[-1],2)=""H3"",LEFT(RC[-1],2)=""T5"",LEFT(RC[-1],2)=""T4""),""A"","""")"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("B").Select
    Selection.Delete Shift:=xlToLeft

    Range("B1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[-1],2)=""H1"",CONCATENATE(R[1]C[-1],R[2]C[-1],R[3]C[-1]),"""")"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1), Array(5, 1), Array(6, 1), _
        Array(7, 1), Array(8, 1), Array(9, 1), Array(10, 1), Array(11, 1), Array(12, 1), Array(13, 1 _
        )), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-2],RC[-1])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "C1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A:B").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("A").Replace What:=";", Replacement:="_"
    Columns("A").Replace What:="", Replacement:=";"

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=True, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1), Array(5, 1), Array(6, 1), _
        Array(7, 1), Array(8, 1), Array(9, 1), Array(10, 1), Array(11, 1), Array(12, 1), Array(13, 1 _
        ), Array(14, 1), Array(15, 1), Array(16, 1), Array(17, 1), Array(18, 1), Array(19, 1), Array _
        (20, 1), Array(21, 1), Array(22, 1), Array(23, 1), Array(24, 1), Array(25, 1)), _
        TrailingMinusNumbers:=True

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("B:F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C:G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D").Select
    Selection.Delete Shift:=xlToLeft
    Columns("E:J").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("B").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Cut Destination:=Columns("I")
    Columns("C").Select
    Selection.Cut Destination:=Columns("J")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")
    
    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True
    
    Columns("D").Select
    Selection.ClearContents
    
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""",RC[-2],RC[-1])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.ClearContents

    Columns("D").Select
    Selection.Cut Destination:=Columns("C")

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=RC[-2],"""",RC[-2])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.ClearContents

    Columns("D").Select
    Selection.Cut Destination:=Columns("B")

    Columns("E").Select
    Selection.ClearContents

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "first name"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middle name"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "last name"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "amount"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "description"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    
    
End Sub


