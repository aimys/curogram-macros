Sub Main()

    Columns("G").Select
    Selection.Delete Shift:=xlToLeft
    Columns("Q:AE").Select
    Selection.Delete Shift:=xlToLeft
    Columns("R:W").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J:N").Select
    Selection.Cut Destination:=Columns("S:W")

    Rows("1:1").Select
    Selection.Delete Shift:=xlUp

    Range("X1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-5],"" "",RC[-4],"" "",RC[-3],"" "",RC[-2],"" "",RC[-1])"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 6), "X1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("S:W").Select
    Selection.Delete Shift:=xlToLeft
    Columns("A").Select
    Selection.Cut Destination:=Columns("K")
    Columns("B:D").Select
    Selection.Cut Destination:=Columns("A:C")
    Columns("E").Select
    Selection.ClearContents

    Columns("F").Select
    Selection.Cut Destination:=Columns("D")
    Columns("O:P").Select
    Selection.Cut Destination:=Columns("E:F")
    Columns("G:H").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("Q").Select
    Selection.Cut Destination:=Columns("N")
    Columns("S").Select
    Selection.Cut Destination:=Columns("L")
    Columns("I").Select
    Selection.Cut Destination:=Columns("M")

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    'clean up phones
    Columns("E:F").Replace What:=" ", Replacement:=""
    Columns("E:F").Replace What:="-", Replacement:=""
    Columns("E:F").Replace What:="(", Replacement:=""
    Columns("E:F").Replace What:=")", Replacement:=""

    'Formatting Phone Columns
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-3]="""","""",TEXT(RC[-1],""+1 (###) ### ####""))"
    Columns("F").Select
    Selection.FillDown
    Columns("F").Select
    Columns("F").Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft
    
    'Formatting Phone Columns
    Columns("G").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("G1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[-4]="""","""",TEXT(RC[-1],""+1 (###) ### ####""))"
    Columns("G").Select
    Selection.FillDown
    Columns("G").Select
    Columns("G").Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("G").Select
    Selection.Delete Shift:=xlToLeft

    Columns("E").Select
    Selection.TextToColumns Destination:=Range("E1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1)), TrailingMinusNumbers:= _
        True
        
    Columns("F").Select
    Selection.TextToColumns Destination:=Range("F1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1)), TrailingMinusNumbers:= _
        True

    Columns("E:F").Replace What:="+1 ()", Replacement:=""

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("O1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 2), "Q1").Select
    Selection.FillDown

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-5]=""NP"",""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("M").Replace What:="Hosp Proc", Replacement:=""

    Columns("M").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    Range("L2").Select
    
    

    
End Sub
