Sub Main()

    Columns("K").Select
    Selection.ClearContents

    Columns("A").Select
    Selection.Cut Destination:=Columns("K")
    Columns("C").Select
    Selection.Cut Destination:=Columns("A")
    Columns("B").Select
    Selection.Cut Destination:=Columns("C")
    Columns("D").Select
    Selection.Cut Destination:=Columns("B")
    Columns("J").Select
    Selection.Cut Destination:=Columns("D")
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft

    Columns("I").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Columns("S").Select
    Selection.Delete Shift:=xlToLeft

    Columns("S:W").Select
    Selection.Delete Shift:=xlToLeft

    Columns("R").Select
    Selection.Delete Shift:=xlToLeft
    Columns("M:N").Select
    Selection.ClearContents

    Columns("O").Select
    Selection.Cut Destination:=Columns("N")
    Columns("P").Select
    Selection.Cut Destination:=Columns("R")
    Columns("L").Select
    Selection.Cut Destination:=Columns("O")
    
    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlFixedWidth, _
        FieldInfo:=Array(Array(0, 1), Array(10, 1)), TrailingMinusNumbers:=True

    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("S1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=""NEWPAT"",""true"",""false"")"
    Range("R1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "S1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("R").Select
    Selection.Cut Destination:=Columns("AZ")

    Columns("R").Select
    Selection.Delete Shift:=xlToLeft

    Columns("P").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[-1],""mm.dd.yyyy"")"
    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "P1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("O").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=TEXT(RC[1],""mm.dd.yyyy"")"
    Range("C1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft

    Columns("D").Replace What:="01.00.1900", Replacement:=""

    Columns("N").Select
    Selection.Copy
    Columns("S").Select
    ActiveSheet.Paste
    Application.CutCopyMode = False

    Dim i As Integer, day As String, Time As String, dateTime As String, Last As Integer, Name As String, Phone As String
    Last = Cells(Rows.Count, 1).End(xlUp).Row
    For i = 1 To Last
    dateTime = Cells(i, 15).Value
    day = WeekdayName(Weekday(Replace(dateTime, ".", "/")))
    Cells(i, 20) = day
    Next i

    Last = Cells(Rows.Count, 1).End(xlUp).Row
    For i = 1 To Last
    Time = Cells(i, 15).Value
    Time = Format(Time, "hh:mm AM/PM")
    Cells(i, 15) = Time
    Next i

    Last = Cells(Rows.Count, 1).End(xlUp).Row
    For i = 1 To Last
    Phone = Cells(i, 5).Value
    If Phone <> "" Then
        Phone = Format(Phone, "+1 (###) ### ####")
        Cells(i, 5) = Phone
    End If
    Next i

    Last = Cells(Rows.Count, 1).End(xlUp).Row
    For i = 1 To Last
    Phone = Cells(i, 7).Value
    If Phone <> "" Then
        Phone = Format(Phone, "+1 (###) ### ####")
        Cells(i, 7) = Phone
    End If
    Next i

    Columns("P").EntireColumn.AutoFit

    Last = Cells(Rows.Count, 1).End(xlUp).Row
   
    For i = 1 To Last
    Time = Cells(i, 16).Text()
    Name = Cells(i, 19).Value()
    day = Cells(i, 20).Text()

    Select Case day
        Case "Monday"
            Select Case Name
                Case "ARMIN"
                    IF TimeValue(Time) >= TimeValue("08:00 AM") And TimeValue(Time) <= TimeValue("12:00 PM") Then
                        Cells(i, 21) = "Encino Office"
                    ElseIf TimeValue(Time) >= TimeValue("01:00 PM") And TimeValue(Time) <= TimeValue("05:00 PM") Then
                        Cells(i, 21) = "WEST HILLS"
                    Else
                        Cells(i, 21) = ""
                    End If
                Case "COHEN"
                    Cells(i, 21) = "WEST HILLS"
                Case "Hersh"
                    IF TimeValue(Time) >= TimeValue("08:00 AM") And TimeValue(Time) <= TimeValue("12:00 PM") Then
                        Cells(i, 21) = "WEST HILLS"
                    ElseIf TimeValue(Time) >= TimeValue("01:00 PM") And TimeValue(Time) <= TimeValue("05:00 PM") Then
                        Cells(i, 21) = "Westlake Village"
                    Else
                        Cells(i, 21) = ""
                    End If
                Case "Panossia"
                    IF TimeValue(Time) >= TimeValue("08:00 AM") And TimeValue(Time) <= TimeValue("12:00 PM") Then
                        Cells(i, 21) = "Westlake Village"
                    ElseIf TimeValue(Time) >= TimeValue("01:00 PM") And TimeValue(Time) <= TimeValue("05:00 PM") Then
                        Cells(i, 21) = "WEST HILLS"
                    Else
                        Cells(i, 21) = ""
                    End If
                Case Else
                    Cells(i, 21) = ""
            End Select
        Case "Tuesday"
            Select Case Name
                Case "ARMIN"
                    Cells(i, 21) = "WEST HILLS"
                Case "COHEN"
                    Cells(i, 21) = "WEST HILLS"
                Case "Hersh"
                    Cells(i, 21) = "WEST HILLS"
                Case "Panossia"
                    Cells(i, 21) = "Westlake Village"
                Case Else
                    Cells(i, 21) = ""
            End Select
        Case "Wednesday"
            Select Case Name
                Case "ARMIN"
                    Cells(i, 21) = "Encino Office"
                Case "COHEN"
                    Cells(i, 21) = "WEST HILLS"
                Case "Hersh"
                    Cells(i, 21) = "WEST HILLS"
                Case "Panossia"
                    Cells(i, 21) = "Westlake Village"
                Case Else
                    Cells(i, 21) = ""
            End Select
        Case "Thursday"
            Select Case Name
                Case "ARMIN"
                    Cells(i, 21) = "WEST HILLS"
                Case "COHEN"
                    Cells(i, 21) = "WEST HILLS"
                Case "Hersh"
                    Cells(i, 21) = "WEST HILLS"
                Case "Panossia"
                    Cells(i, 21) = "Westlake Village"
                Case Else
                    Cells(i, 21) = ""
            End Select
        Case "Friday"
            Select Case Name
                Case "ARMIN"
                    Cells(i, 21) = "Encino Office"
                Case "COHEN"
                    Cells(i, 21) = "WEST HILLS"
                Case "Hersh"
                    IF TimeValue(Time) >= TimeValue("08:00 AM") And TimeValue(Time) <= TimeValue("12:00 PM") Then
                        Cells(i, 21) = "WEST HILLS"
                    ElseIf TimeValue(Time) >= TimeValue("01:00 PM") And TimeValue(Time) <= TimeValue("05:00 PM") Then
                        Cells(i, 21) = "Westlake Village"
                    Else
                        Cells(i, 21) = ""
                    End If
                Case "Panossia"
                    IF TimeValue(Time) >= TimeValue("08:00 AM") And TimeValue(Time) <= TimeValue("12:00 PM") Then
                        Cells(i, 21) = "Westlake Village"
                    ElseIf TimeValue(Time) >= TimeValue("01:00 PM") And TimeValue(Time) <= TimeValue("05:00 PM") Then
                        Cells(i, 21) = "WEST HILLS"
                    Else
                        Cells(i, 21) = ""
                    End If
                Case Else
                    Cells(i, 21) = ""
            End Select
        Case "Saturday"
            Cells(i, 21) = ""
        Case "Sunday"
            Cells(i, 21) = ""
        Case Else
            Cells(i, 21) = ""
    End Select

    Next i

    Columns("U").Select
    Selection.Cut Destination:=Columns("L")
    Columns("S:T").Select
    Selection.Delete Shift:=xlToLeft

    Columns("AW").Select
    Selection.Cut Destination:=Columns("M")

    Columns("N").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[1]=""Cecilio"",""Encino Office"",RC[-2])"
    Range("O1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "N1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("L").Select
    Selection.ClearContents
    Columns("N").Select
    Selection.Cut Destination:=Columns("L")

    Columns("N").Select
    Selection.Delete Shift:=xlToLeft

    Columns("L").Replace What:="0", Replacement:=""
    Columns("P").Replace What:=" ", Replacement:=""

    Columns("A").Replace What:="BLOCK", Replacement:=""

    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
    
End Sub

