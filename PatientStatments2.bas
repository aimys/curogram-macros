Sub Main()

    Range("H1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-7],RC[-6],RC[-5],RC[-4],RC[-3],RC[-2],RC[-1])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 7), "H1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A:G").Select
    Selection.Delete Shift:=xlToLeft

    Range("B1").Select
    ActiveCell.FormulaR1C1 = "=IF(OR(LEFT(RC[-1],2)=""H2"",LEFT(RC[-1],2)=""H3"",LEFT(RC[-1],2)=""D1""),""A"","""")"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "B1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("B").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("A1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[1],2)=""H2"",CONCATENATE(RC[1],R[1]C[1]),"""")"
    Range("B1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "A1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
    
    Columns("A").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    ActiveCell.FormulaR1C1 = "=R[-1]C"
    Range("A3").Activate
    Selection.FillDown
    Columns("A").Select
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False
    
    Columns("B").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove

    Range("B1").Select
    ActiveCell.FormulaR1C1 = "=IF(LEFT(RC[1],2)=""D1"",""A"","""")"
    Range("C1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, -1), "B1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.TextToColumns Destination:=Range("B1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    Columns("B").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    Columns("B").Select
    Selection.Delete Shift:=xlToLeft

    Range("C1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-2],RC[-1])"
    Range("B1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "C1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("A:B").Select
    Selection.Delete Shift:=xlToLeft

    Columns("A").Replace What:=";", Replacement:="_"
    Columns("A").Replace What:="", Replacement:=";"

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=False, _
        Semicolon:=True, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1), Array(5, 1), Array(6, 1), _
        Array(7, 1), Array(8, 1), Array(9, 1), Array(10, 1), Array(11, 1), Array(12, 1), Array(13, 1 _
        ), Array(14, 1), Array(15, 1), Array(16, 1), Array(17, 1), Array(18, 1), Array(19, 1), Array _
        (20, 1), Array(21, 1), Array(22, 1), Array(23, 1), Array(24, 1), Array(25, 1)), _
        TrailingMinusNumbers:=True

    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("B:H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("C").Select
    Selection.Delete Shift:=xlToLeft
    Columns("D:E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("H:J").Select
    Selection.Delete Shift:=xlToLeft
    Columns("I:P").Select
    Selection.Delete Shift:=xlToLeft
    
    Columns("A").Select
    Selection.Cut Destination:=Columns("I")
    Columns("E:G").Select
    Selection.Cut Destination:=Columns("J:L")
    Columns("C").Select
    Selection.Cut Destination:=Columns("M")
    Columns("H").Select
    Selection.Cut Destination:=Columns("N")
    Columns("D").Select
    Selection.Cut Destination:=Columns("O")
    Columns("B").Select
    Selection.Cut Destination:=Columns("A")

    Columns("A").Select
    Selection.TextToColumns Destination:=Range("A1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=True, Tab:=False, _
        Semicolon:=False, Comma:=False, Space:=True, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1)), TrailingMinusNumbers:=True
    
    Columns("D").Select
    Selection.ClearContents
    
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]="""",RC[-2],RC[-1])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("C").Select
    Selection.ClearContents

    Columns("D").Select
    Selection.Cut Destination:=Columns("C")

    Range("D1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-1]=RC[-2],"""",RC[-2])"
    Range("A1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 3), "D1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    Columns("B").Select
    Selection.ClearContents

    Columns("D").Select
    Selection.Cut Destination:=Columns("B")

    Columns("E").Select
    Selection.ClearContents

    Columns("M").Replace What:="/", Replacement:="."

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "charges"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "adjust"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "receipts"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "serviceDate"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "amount"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "description"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "newPatient"
        
End Sub


