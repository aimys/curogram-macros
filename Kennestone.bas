Sub Main()

    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("J:N").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L:Z").Select
    Selection.Delete Shift:=xlToLeft
    Columns("M:R").Select
    Selection.Delete Shift:=xlToLeft

    Columns("K").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    
    Columns("A").Select
    Selection.Cut Destination:=Columns("K")
    Columns("B:E").Select
    Selection.Cut Destination:=Columns("A:D")
    Columns("J").Select
    Selection.Cut Destination:=Columns("E")
    Columns("M").Select
    Selection.Cut Destination:=Columns("N")
    Columns("I").Select
    Selection.Cut Destination:=Columns("M")
    Columns("G:H").Select
    Selection.Cut Destination:=Columns("O:P")
    Columns("F").Select
    Selection.Cut Destination:=Columns("J")
    Columns("L").Select
    Selection.Cut Destination:=Columns("F")
    Columns("J").Select
    Selection.Cut Destination:=Columns("L")
    
    Rows("1").Select
    Selection.Delete Shift:=xlUp

    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Columns("D").Replace What:="/", Replacement:="."
    Columns("O").Replace What:="/", Replacement:="."

    Columns("P").Replace What:=" ", Replacement:=""

    Range("R1").Select
    ActiveCell.FormulaR1C1 = "=IF(RC[-5]=""NP"",""true"",""false"")"
    Range("Q1").Select
    Selection.End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "R1").Select
    Selection.FillDown
    Selection.Copy
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Application.CutCopyMode = False

    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove

    ' Changing Header Names
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "location"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("R1").Select
    ActiveCell.FormulaR1C1 = "newPatient"

End Sub
