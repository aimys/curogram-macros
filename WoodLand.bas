Sub Main()
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("J").Replace What:="SURGERY", Replacement:=""
    Columns("J").Select
    Selection.SpecialCells(xlCellTypeBlanks).Select
    Selection.EntireRow.Delete

    'Deleting Columns
    Columns("E").Select
    Selection.Delete Shift:=xlToLeft
    Columns("F").Select
    Selection.Delete Shift:=xlToLeft

    Columns("H").Select
    Selection.Cut Destination:=Columns("AZ")

    Columns("H").Select
    Selection.Delete Shift:=xlToLeft
    Columns("O:Z").Select
    Columns("O:AC").Select
    Selection.Delete Shift:=xlToLeft
    Columns("P:U").Select
    Selection.Delete Shift:=xlToLeft
    
    'Replacing Columns
    Columns("F:L").Select
    Selection.Cut Destination:=Columns("P:V")
    Columns("M:N").Select
    Selection.Cut Destination:=Columns("F:G")
    Columns("A").Select
    Selection.Cut Destination:=Columns("L")
    Columns("A").Select
    Selection.Delete Shift:=xlToLeft
    Columns("L:M").Select
    Selection.Delete Shift:=xlToLeft
    
    'Formatting dob Column
    Columns("D").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("D1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],"" "",""/""),""mm.dd.yyyy""))"
    Columns("D").Select
    Selection.FillDown
    Selection.Copy
    Columns("E").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("D").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Concatinating Address Column
    Range("T1").Select
    ActiveCell.FormulaR1C1 = "=CONCATENATE(RC[-5],"" "",RC[-4],"" "",RC[-3],"" "",RC[-2],"" "",RC[-1])"
    Columns("T").Select
    Selection.FillDown
    Selection.Copy
    Columns("U").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("O:T").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Deleting First Row
    Rows("1:1").Select
    Selection.Delete Shift:=xlUp
    
    'Replacing Columns
    Columns("N").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("L").Select
    Selection.Cut Destination:=Columns("N")
    Columns("P").Select
    Selection.Cut Destination:=Columns("L")
    Columns("L").Select
    Columns("O").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Columns("M").Select
    Selection.Cut Destination:=Columns("O")
    Columns("O").Select
    
    'Formatting Phone1 Columns
    Columns("E").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("E1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],""-"",""""),""+1 (###) ### ####""))"
    Columns("E").Select
    Selection.FillDown
    Selection.Copy
    Columns("F").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("E").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Formatting Phone2 Columns
    Columns("F").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("F1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],""-"",""""),""+1 (###) ### ####""))"
    Columns("F").Select
    Selection.FillDown
    Selection.Copy
    Columns("G").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("F").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Formatting appointment date Column
    Columns("O").Select
    Selection.Insert Shift:=xlToRight, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("O1").Select
    ActiveCell.FormulaR1C1 = _
        "=IF(RC[1]="""","""",TEXT(SUBSTITUTE(RC[1],"" "",""/""),""mm.dd.yyyy""))"
    Columns("O").Select
    Selection.FillDown
    Selection.Copy
    Columns("P").Select
    Selection.PasteSpecial Paste:=xlPasteValues, Operation:=xlNone, SkipBlanks _
        :=False, Transpose:=False
    Columns("O").Select
    Application.CutCopyMode = False
    Selection.Delete Shift:=xlToLeft
    
    'Refreshing Grid
    Columns("D").Select
    Selection.TextToColumns Destination:=Range("D1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True
        
    Columns("E").Select
    Selection.TextToColumns Destination:=Range("E1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1)), TrailingMinusNumbers:= _
        True
        
    Columns("F").Select
    Selection.TextToColumns Destination:=Range("F1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(Array(1, 1), Array(2, 1), Array(3, 1), Array(4, 1)), TrailingMinusNumbers:= _
        True
    
    Columns("O").Select
    Selection.TextToColumns Destination:=Range("O1"), DataType:=xlDelimited, _
        TextQualifier:=xlDoubleQuote, ConsecutiveDelimiter:=False, Tab:=True, _
        Semicolon:=False, Comma:=False, Space:=False, Other:=False, FieldInfo _
        :=Array(1, 1), TrailingMinusNumbers:=True

    'adding data to duration column
    Range("Q1").Activate
    ActiveCell.FormulaR1C1 = "30"
    Range("P1").End(xlDown).Select
    Range(ActiveCell.Offset(0, 1), "Q1").Select
    Selection.FillDown

    Columns("W").Select
    Selection.Cut Destination:=Columns("M")
    
    ' Adding Header Row
    Rows("1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    
    ' Changing Header Names
    ActiveCell.FormulaR1C1 = ""
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "firstName"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "middleName"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "lastName"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "dob"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "phone1"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "phone2"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "phone3"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "phone4"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "email1"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "email2"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "mrn"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "address"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "service"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "physicianName"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "appointmentDate"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "appointmentTime"
    Range("Q1").Select
    ActiveCell.FormulaR1C1 = "duration"
    Range("L2").Select
End Sub
